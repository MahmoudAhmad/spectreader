#include "activespectrometers.h"
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QSize>
#include <QDebug>
#include <QWidget>
#include <QStyle>
#include <QColor>
#include <QPalette>
#include <QSettings>

activeSpectrometers::activeSpectrometers(QWidget *parent) : QWidget(parent)
{

    connect(this,SIGNAL(setNewGraphsNames()),uiNameSpace::plotSpectraObj,SLOT(setNewGraphsNames()));
    connect(uiNameSpace::mainToolBarObj,SIGNAL(updateActiveSpects()),this,SLOT(disableUnavailableSpectrometers()));
    connect(uiNameSpace::mainToolBarObj,SIGNAL(exportSpectrometerParamters()),this,SLOT(exportSpectrometerParamters()));
    connect(uiNameSpace::mainToolBarObj,SIGNAL(readSpectrometerParamters()),this,SLOT(readSpectrometerParamters()));

    QGroupBox *activeSpectrometersGroubBox=new QGroupBox("Active Spectrometers");
    activeSpectrometersGroubBox->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");
    activeSpectrometersGroubBox->setFont(uiNameSpace::WinXFontM);


    QGridLayout *activeSpectrometersGridLayout=new QGridLayout;

// sample type
    QLabel *runSampleLabel=new QLabel("Sample Type:");
    runSampleLabel->setFont(uiNameSpace::WinXFontM);
    sampleLineEdit=new QLineEdit("sampleType");
    sampleLineEdit->setAlignment(Qt::AlignCenter);
    sampleLineEdit->setFont(uiNameSpace::WinXFontM);
    uiNameSpace::sampleType=sampleLineEdit->text();
    connect(sampleLineEdit,SIGNAL(textChanged(QString)),this,SLOT(setSampleType()));

    QHBoxLayout *sampleHLayout=new QHBoxLayout;
    sampleHLayout->addWidget(runSampleLabel);
    sampleHLayout->addWidget(sampleLineEdit);

    QLabel *spectChannelLabels=new QLabel("Ch");
    spectChannelLabels->setFont(uiNameSpace::WinXFontM);
    QLabel *spectNamesLabel=new QLabel("Ch. Name");
    spectNamesLabel->setFont(uiNameSpace::WinXFontM);
    QLabel *spectExposuresLabel=new QLabel("Exposure (ms)");
    spectExposuresLabel->setFont(uiNameSpace::WinXFontM);


    QHBoxLayout *activeSpectraLabelsHBoxLayout=new QHBoxLayout;
    activeSpectraLabelsHBoxLayout->addWidget(spectChannelLabels,0,Qt::AlignCenter);
    activeSpectraLabelsHBoxLayout->addWidget(spectNamesLabel,1,Qt::AlignCenter);
    activeSpectraLabelsHBoxLayout->addWidget(spectExposuresLabel,1,Qt::AlignCenter);

    QVBoxLayout *activeSpectrometersVBoxLayout=new QVBoxLayout;
    activeSpectrometersVBoxLayout->addLayout(sampleHLayout);
    activeSpectrometersVBoxLayout->addLayout(activeSpectraLabelsHBoxLayout);

    QLabel *spectrometersLabel[8];
    QHBoxLayout *activeSpectrometersHBoxLayout[8];

    for (int i=0;i<8;i++)
    {
        spectrometersLabel[i]=new QLabel("("+QString::number(i)+")");
        spectrometersLabel[i]->setFont(uiNameSpace::WinXFontM);
        spectrometersNameLineEdit[i]=new QLineEdit("Ch"+QString::number(i));
        spectrometersNameLineEdit[i]->setFont(uiNameSpace::WinXFontM);
        spectrometersNameLineEdit[i]->setAlignment(Qt::AlignCenter);
        spectrometersExposureLineEdit[i]=new QLineEdit("1");
        spectrometersExposureLineEdit[i]->setAlignment(Qt::AlignCenter);
        spectrometersExposureLineEdit[i]->setFont(uiNameSpace::WinXFontM);

        uiNameSpace::spectrometerName[i]=spectrometersNameLineEdit[i]->text();
        uiNameSpace::spectrometerExposureInmSec[i]=spectrometersExposureLineEdit[i]->text().toInt();

        connect(spectrometersNameLineEdit[i],SIGNAL(textChanged(QString)),this,SLOT(setSpectrometersName()));
        connect(spectrometersExposureLineEdit[i],SIGNAL(textChanged(QString)),this,SLOT(setSpectrometersExposureTime()));

        activeSpectrometersHBoxLayout[i]=new QHBoxLayout;
        activeSpectrometersHBoxLayout[i]->addWidget(spectrometersLabel[i]);
        activeSpectrometersHBoxLayout[i]->addWidget(spectrometersNameLineEdit[i]);
        activeSpectrometersHBoxLayout[i]->addWidget(spectrometersExposureLineEdit[i]);
        activeSpectrometersVBoxLayout->addLayout(activeSpectrometersHBoxLayout[i]);
    }
    this->disableUnavailableSpectrometers();
    activeSpectrometersGroubBox->setLayout(activeSpectrometersVBoxLayout);
    activeSpectrometersGridLayout->addWidget(activeSpectrometersGroubBox);
    activeSpectrometersGroubBox->setFixedSize(300,300);
    this->setLayout(activeSpectrometersGridLayout);

}

activeSpectrometers::~activeSpectrometers()
{

}

void activeSpectrometers::setSampleType()
{
    uiNameSpace::sampleType=sampleLineEdit->text();
}

void activeSpectrometers::setSpectrometersName()
{
    for (int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        uiNameSpace::spectrometerName[i]=spectrometersNameLineEdit[i]->text();
    }
    emit setNewGraphsNames();
}


void activeSpectrometers::setSpectrometersExposureTime()
{
    for (int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        if(spectrometersExposureLineEdit[i]->text().toInt() <=0 or spectrometersExposureLineEdit[i]->text().toInt() > 1800000)
        {
            spectrometersExposureLineEdit[i]->setText(QString::number(uiNameSpace::spectrometerExposureInmSec[i]/1000));
            spectrometersExposureLineEdit[i]->setPalette(paletteYellow);
        }
        else
        {
            uiNameSpace::spectrometerExposureInmSec[i]=spectrometersExposureLineEdit[i]->text().toInt();
            spectrometersExposureLineEdit[i]->setPalette(paletteWhite);
        }
    }
}

void activeSpectrometers::disableUnavailableSpectrometers()
{

    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        spectrometersNameLineEdit[i]->setEnabled(true);
        spectrometersExposureLineEdit[i]->setEnabled(true);
    }

    for(int i=uiNameSpace::noSpectrometers;i<8;i++)
    {
        spectrometersNameLineEdit[i]->setDisabled(true);
        spectrometersExposureLineEdit[i]->setDisabled(true);
    }
}

void activeSpectrometers::exportSpectrometerParamters()
{

    uiNameSpace::settings->beginGroup("Active Spectromters");

    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {

        uiNameSpace::settings->setValue("CH"+QString::number(i)+" Name",uiNameSpace::spectrometerName[i]);
        uiNameSpace::settings->setValue("CH"+QString::number(i)+" Exposure",QString::number(uiNameSpace::spectrometerExposureInmSec[i]));

    }

    uiNameSpace::settings->endGroup();
    uiNameSpace::settings->sync();
}

void activeSpectrometers::readSpectrometerParamters()
{
    uiNameSpace::settings->beginGroup("Active Spectromters");

    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {

        spectrometersNameLineEdit[i]->setText(uiNameSpace::settings->value("CH"+QString::number(i)+" Name",spectrometersNameLineEdit[i]->text()).toString());
        spectrometersExposureLineEdit[i]->setText(uiNameSpace::settings->value("CH"+QString::number(i)+" Exposure",spectrometersExposureLineEdit[i]->text()).toString());

    }

    uiNameSpace::settings->endGroup();
}
