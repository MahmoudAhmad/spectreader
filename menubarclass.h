#ifndef MENUBARCLASS_H
#define MENUBARCLASS_H
#include <QMenuBar>
#include <QString>
#include <QSettings>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QDialog>

#include "plotspectraclass.h"
namespace uiNameSpace {
extern QString outputFilesDir;
extern QString spectC_Code[8],preferedSpectsOrder[8];

extern plotSpectraClass *plotSpectraObj;
} // namespace uiNameSpace


class menuBarClass : public QMenuBar
{
    Q_OBJECT

    QSettings *SpecPref;
    QLabel *channelsLabel[8];
    QComboBox *channelsCCodeComboBox[8];
    QList<QString> spectAvaialbleForAssignment[8];
    QDialog *channelsDialog;
    QPushButton *saveAndQuitPushButton,*resetPushButton;




public:
    menuBarClass();
    ~menuBarClass();
signals:
    void viewLegend(bool);
    void closeApplication();

public slots:
    void closeAppSignal();
    void assignSameChannelsAtStartup();
private slots:
    void displayAboutInfo();
    void assignSameChannels();
    void updateAssignmentsOfSpectrometers(int);
    void saveAndQuitFunction();
    void resetSpectrometersPreferenceFunction();

};

#endif // MENUBARCLASS_H
