#ifndef RUNPARAMETERSCLASS_H
#define RUNPARAMETERSCLASS_H
#include <QComboBox>
#include <QLineEdit>
#include "toolBarclass.h"
#include <QFont>

QT_BEGIN_NAMESPACE

namespace uiNameSpace {

extern bool isAcquiringLightNow;

extern bool applyNewParametersBool;
extern unsigned long totalRunTimeInmSec;
extern unsigned int totalRunTimeUnits;
extern unsigned long readingIntervalInmSec;
extern unsigned int readingIntervalUnits;
extern QString spectrometerName[8];
extern unsigned long spectrometerExposureInmSec[8];


extern int const noPixels;
extern int xBeginPixel[8],xEndPixel[8];
extern float wavelengthToPixelFactor[8];
extern float xMinGlobalWavelength,xMaxGlobalWavelength;
extern float beginScanFrequency,endScanFrequency;


extern bool applyNewParametersSwitch;

extern bool isModeMean;
extern int scansAveraged;
extern int smoothingType,smoothingValue;

extern toolBarClass *mainToolBarObj;

extern double fcoeffA[8][4];
extern double fcoeffB[8][4];
extern QFont WinXFontS,WinXFontM,WinXFontL; // fonts changes with operating systems.
extern QSettings* settings;
} // namespace uiNameSpace


QT_END_NAMESPACE

class runParametersClass : public QWidget
{
    Q_OBJECT

    QLineEdit *totalRunTimeLineEdit = new QLineEdit("2");
    QComboBox *totalRunTimeComboBox =new QComboBox;
    QLineEdit *readingIntervalLineEdit = new QLineEdit("500");
    QComboBox *readingIntervalComboBox =new QComboBox;
    QLineEdit *beginScanRangeLineEdit;
    QLineEdit *endScanRangeLineEdit;
    QComboBox *scanModeComboBox;
    QLineEdit *scansAveragedLineEdit;
    QComboBox *smoothingTypeComboBox;
    QLineEdit *smoothingValueLineEdit;

    QPalette paletteWhite,paletteYellow;

public:
    explicit runParametersClass(QWidget *parent = 0);
    ~runParametersClass();

signals:
    void updateNewFrequencyRange();

public slots:
    void setTotalRunTime();
    void setReadingInterval();
    void setFrequencyRange();
    void setScanMode();
    void setScansAveragedPerReading();
    void setSmoothingType();
    void setSmoothingValue();
private slots:
    void warnUserwithChangeTotalRunTime();
    void warnUserChangedReadingInterval();
    void warnUserBeginFrequencyChange();
    void warnUserEndFrequencyChange();
    void warnUsersScansAveragedPerReadingChanged();
    void warnUserSmoothingValueChanged();
    void exportRunParameters();
    void readRunParameters();

};

#endif // RUNPARAMETERSCLASS_H
