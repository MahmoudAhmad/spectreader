#include "menubarclass.h"
#include <QFont>
#include <QCheckBox>
#include <QMenu>
#include <QMainWindow>
#include <QGridLayout>
#include <QDialog>
#include <QList>
#include <QPalette>

menuBarClass::menuBarClass()
{

    QFont menuFonts("times",10);

    QMenu *fileMenu=this->addMenu("&File");

    QAction *assignChannels=new QAction("&Assign Channels",this);
    assignChannels->setFont(menuFonts);
    fileMenu->addAction(assignChannels);
    connect(assignChannels,SIGNAL(triggered()),this,SLOT(assignSameChannels()));

    QAction *quitApp=new QAction("&Quit",this);
    connect(quitApp,SIGNAL(triggered()),this,SLOT(closeAppSignal()));
    quitApp->setFont(menuFonts);
    fileMenu->addAction(quitApp);

    QMenu *optionObj=this->addMenu("&Plot Options");
    QAction *viewLegend=new QAction("&view Legend",this);
    viewLegend->setCheckable(true);
    viewLegend->setChecked(true);
    connect(viewLegend,SIGNAL(triggered(bool)),uiNameSpace::plotSpectraObj,SLOT(hideLegend(bool)));
    optionObj->addAction(viewLegend);
    optionObj->setFont(menuFonts);

    QMenu *aboutMenu=this->addMenu("&About");
    QAction *aboutAction=new QAction("&About",this);
    connect(aboutAction,SIGNAL(triggered()),this,SLOT(displayAboutInfo()));
    aboutMenu->setFont(menuFonts);
    aboutMenu->addAction(aboutAction);

    this->setFont(menuFonts);

    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        for(int j=0;j<uiNameSpace::noSpectrometers;j++)
        {
            spectAvaialbleForAssignment[i].append(uiNameSpace::spectC_Code[j]);
        }
    }
}

menuBarClass::~menuBarClass()
{

}

void menuBarClass::closeAppSignal()
{
    emit closeApplication();
}

void menuBarClass::displayAboutInfo()
{
    QMessageBox::about(this,"About","Spectrometer Reader V:1.2");
}

void menuBarClass::assignSameChannels()
{
    // start the dialogue
    channelsDialog=new QDialog;
    channelsDialog->setModal(true);
    channelsDialog->setWindowTitle("Channel to Spectrometer assignement");

    QGridLayout *channelsGridLayout=new QGridLayout;
    QVBoxLayout *channelsVLayout=new QVBoxLayout(channelsDialog);

    saveAndQuitPushButton=new QPushButton;
    saveAndQuitPushButton->setText("Save & Quit");

    resetPushButton=new QPushButton;
    resetPushButton->setText("Reset");


    // build the labels and the comboboxes
    for(int i=0;i<8;i++)
    {
        channelsLabel[i]=new QLabel;
        channelsCCodeComboBox[i]=new QComboBox;
        channelsCCodeComboBox[i]->addItems(spectAvaialbleForAssignment[i]);
        connect(channelsCCodeComboBox[i],SIGNAL(currentIndexChanged(int)),this,SLOT(updateAssignmentsOfSpectrometers(int)));
        channelsLabel[i]->setText("Ch"+QString::number(i)+"  assigned to spectrometer with c_code:");
        channelsGridLayout->addWidget(channelsLabel[i],i,0,Qt::AlignCenter);
        channelsGridLayout->addWidget(channelsCCodeComboBox[i],i,1,Qt::AlignCenter);
    }

    // check if the preferences file exist
    if(!QFile(QApplication::applicationDirPath()+"/SpecPreferences.ini").exists()) // if does not exists
    {
        // create ini file
        SpecPref=new QSettings(QApplication::applicationDirPath()+"/SpecPreferences.ini", QSettings::IniFormat);

        // assign spect automatically
        for(int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            channelsCCodeComboBox[i]->setCurrentIndex(i);
        }
        for(int i=uiNameSpace::noSpectrometers;i<8;i++)
        {
            channelsCCodeComboBox[i]->setCurrentText("N/A");
        }

        // write preferences to ini file
        SpecPref->beginGroup("Spectrometer_Preferences");
        for(int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            SpecPref->setValue("CH"+QString::number(i)+"=",channelsCCodeComboBox[i]->currentText());
            uiNameSpace::preferedSpectsOrder[i]=(channelsCCodeComboBox[i]->currentText());

        }


        for(int i=uiNameSpace::noSpectrometers;i<8;i++)
        {
            SpecPref->setValue("CH"+QString::number(i)+"=","N/A");
        }
        SpecPref->endGroup();

        delete SpecPref;

    }
    else // if exists
    {
        SpecPref=new QSettings(QApplication::applicationDirPath()+"/SpecPreferences.ini", QSettings::IniFormat);
        SpecPref->beginGroup("Spectrometer_Preferences");
        for(int i=0;i<8;i++)
        {
            channelsCCodeComboBox[i]->setCurrentText(SpecPref->value("CH"+QString::number(i)+"=","NA").toString());
            uiNameSpace::preferedSpectsOrder[i]=(channelsCCodeComboBox[i]->currentText());

        }
        SpecPref->endGroup();

        delete SpecPref;

    }

    QHBoxLayout *buttonsLayout=new QHBoxLayout;
    buttonsLayout->addStretch(2);
    buttonsLayout->addWidget(resetPushButton);
    buttonsLayout->addWidget(saveAndQuitPushButton);

    connect(resetPushButton,SIGNAL(clicked()),this,SLOT(resetSpectrometersPreferenceFunction()));
    connect(saveAndQuitPushButton,SIGNAL(clicked()),this,SLOT(saveAndQuitFunction()));

    for(int i=uiNameSpace::noSpectrometers;i<8;i++)
    {
        channelsLabel[i]->setDisabled(true);
        channelsCCodeComboBox[i]->setDisabled(true);
    }

    channelsVLayout->addLayout(channelsGridLayout);
    channelsVLayout->addLayout(buttonsLayout);

    channelsDialog->show();
}

void menuBarClass::assignSameChannelsAtStartup()
{
    // check if the preferences file exist
    if(!QFile(QApplication::applicationDirPath()+"/SpecPreferences.ini").exists()) // if does not exists
    {
        // assign spect automatically
        for(int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            uiNameSpace::preferedSpectsOrder[i]=uiNameSpace::spectC_Code[i];
        }
    }
    else // if exists
    {
        SpecPref=new QSettings(QApplication::applicationDirPath()+"/SpecPreferences.ini", QSettings::IniFormat);
        SpecPref->beginGroup("Spectrometer_Preferences");
        for(int i=0;i<8;i++)
        {
            uiNameSpace::preferedSpectsOrder[i]=SpecPref->value("CH"+QString::number(i)+"=","NA").toString();
        }
        SpecPref->endGroup();

        delete SpecPref;

    }
}

void menuBarClass::updateAssignmentsOfSpectrometers(int currentIndex)
{
    QPalette warningPalette(channelsCCodeComboBox[currentIndex]->palette());
    QPalette normalPalette(channelsCCodeComboBox[currentIndex]->palette());
    warningPalette.setColor(QPalette::Active, QPalette::Text, Qt::red);
    normalPalette.setColor(QPalette::Active, QPalette::Text, Qt::black);

    //check for repeatness
    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
            if(currentIndex != i)
            {
                if(channelsCCodeComboBox[currentIndex]->currentText()==channelsCCodeComboBox[i]->currentText())
                {
                    channelsCCodeComboBox[currentIndex]->setPalette(warningPalette);
                    channelsCCodeComboBox[i]->setPalette(warningPalette);
                    if(saveAndQuitPushButton->isEnabled())
                        saveAndQuitPushButton->setDisabled(true);

                }
                else
                {
                    channelsCCodeComboBox[currentIndex]->setPalette(normalPalette);
                    channelsCCodeComboBox[i]->setPalette(normalPalette);
                    if(!saveAndQuitPushButton->isEnabled())
                        saveAndQuitPushButton->setEnabled(true);
                }
            }
    }

}

void menuBarClass::resetSpectrometersPreferenceFunction()
{
    // create ini file
    SpecPref=new QSettings(QApplication::applicationDirPath()+"/SpecPreferences.ini", QSettings::IniFormat);

    // assign spect automatically
    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        channelsCCodeComboBox[i]->setCurrentIndex(i);

    }
    for(int i=uiNameSpace::noSpectrometers;i<8;i++)
    {
        channelsCCodeComboBox[i]->setCurrentText("N/A");
    }

    // write preferences to ini file
    SpecPref->beginGroup("Spectrometer_Preferences");
    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        SpecPref->setValue("CH"+QString::number(i)+"=",channelsCCodeComboBox[i]->currentText());
    }
    for(int i=uiNameSpace::noSpectrometers;i<8;i++)
    {
        SpecPref->setValue("CH"+QString::number(i)+"=","N/A");
    }
    SpecPref->endGroup();

    delete SpecPref;
}

void menuBarClass::saveAndQuitFunction()
{
    SpecPref=new QSettings(QApplication::applicationDirPath()+"/SpecPreferences.ini", QSettings::IniFormat);
    // write preferences to ini file
    SpecPref->beginGroup("Spectrometer_Preferences");
    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        SpecPref->setValue("CH"+QString::number(i)+"=",channelsCCodeComboBox[i]->currentText());
        uiNameSpace::preferedSpectsOrder[i]=(channelsCCodeComboBox[i]->currentText());
    }
    for(int i=uiNameSpace::noSpectrometers;i<8;i++)
    {
        SpecPref->setValue("CH"+QString::number(i)+"=","N/A");
    }
    SpecPref->endGroup();

    delete SpecPref;

    channelsDialog->close();
    delete channelsDialog;
}
