#ifndef UI
#define UI
#include "menubarclass.h"
#include "toolBarclass.h"
#include "plotspectraclass.h"
#include <QWidget>
#include <QMainWindow>
#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QProgressBar>
#include <QList>

QT_BEGIN_NAMESPACE

namespace uiNameSpace {
int noSpectrometers;
QString outputFilesDir;

bool applyNewParametersBool=false;
bool applyNewParametersSwitch=false;
bool isAcquiringLightNow=false;

unsigned long totalRunTimeInmSec;
unsigned int totalRunTimeUnits;
unsigned long readingIntervalInmSec;
unsigned int readingIntervalUnits;
QString spectrometerName[8];
QString spectC_Code[8],preferedSpectsOrder[8];

unsigned long spectrometerExposureInmSec[8];

int const noPixels=2048;
int xBeginPixel[8],xEndPixel[8];
float minWavelength[8],maxWavelength[8];
float wavelengthToPixelFactor[8];
float xMinGlobalWavelength,xMaxGlobalWavelength;
float beginScanFrequency,endScanFrequency;
float yMinValue=0.0,yMaxValue=1.0;

bool isModeMean=true;
int scansAveraged;
int smoothingType,smoothingValue;
QString sampleType;

bool initiateSpectrometers();
void shutdownSpectrometers();


QWidget *centralWidgetObj;
menuBarClass *mainMenuBarObj;
toolBarClass *mainToolBarObj;
QStatusBar *mainStatusBarObj;
QProgressBar *mainProgressBarObj;

unsigned short spectOutput[8][2048];
unsigned short spectBackGroundSignal[8][2048];
bool isAcquiringDarkSignalNow=false;
bool printDataHeadings[8];
plotSpectraClass *plotSpectraObj;

bool isBackgroundAcquired=false;
bool isBackgroundSubtracted=false;
bool isBackgroundNeedToBeSubtracted=false;
bool acquireLightOnceNow=false;

// Data Conversion
double fcoeffA[8][4];
double fcoeffB[8][4];

QList<QString> graphsTimeStamp[8];

QString oldFileNames[8];
QFont defaultFont("Helvetica",10);
QFont WinXFontS,WinXFontM,WinXFontL; // fonts changes with operating systems.

QString settingsFileName;
QString settingsFileDir;
QSettings* settings ;

} // namespace uiNameSpace

namespace filters {
bool isThresholdFilterEnabled=false;
int thresholdFilterPercentofPixels=90,thresholdFilterBaseLine=3;

bool isDistanceBasedFilterEnabled=false;

}

QT_END_NAMESPACE

#endif // UI
