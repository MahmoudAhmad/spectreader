#ifndef SPECTROMETERFUNCTIONS_H
#define SPECTROMETERFUNCTIONS_H
#include <QWidget>

QT_BEGIN_NAMESPACE

namespace uiNameSpace {
extern bool applyNewParametersSwitch;

extern int noSpectrometers;
extern const int noPixels;

extern QString spectrometerName[8];
extern QString spectC_Code[8];

extern unsigned long spectrometerExposureInmSec[8];

extern int const noPixels;
extern int xBeginPixel[8],xEndPixel[8];
extern float minWavelength[8],maxWavelength[8];
extern float wavelengthToPixelFactor[8];
extern float xMinGlobalWavelength,xMaxGlobalWavelength;
extern float beginScanFrequency,endScanFrequency;

extern bool isModeMean;
extern int scansAveraged;
extern int smoothingType,smoothingValue;
extern QString sampleType;

extern QString outputFilesDir;

extern bool initiateSpectrometers();
extern void shutdownSpectrometers();

extern double fcoeffA[8][4];
extern double fcoeffB[8][4];

} // namespace uiNameSpace




#endif // SPECTROMETERFUNCTIONS_H
