#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "bwtekusb.h"
#include <QMainWindow>
#include "toolBarclass.h"
#include "runparametersclass.h"
#include "menubarclass.h"
#include <QProgressBar>
#include <QStatusBar>


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void detectOSVERtoConfigSettings();

signals:
    void assignSpectsPreferences();

};

#endif // MAINWINDOW_H
