#ifndef ACTIVESPECTROMETERS_H
#define ACTIVESPECTROMETERS_H
#include <QWidget>
#include <toolBarclass.h>
#include <QLineEdit>
#include <QString>
#include "plotspectraclass.h"

QT_BEGIN_NAMESPACE

namespace uiNameSpace {

extern int noSpectrometers;
extern bool isAcquiringLightNow;

extern QString spectrometerName[8];
extern unsigned long spectrometerExposureInmSec[8];

extern int const noPixels;
extern int xBeginPixel[8],xEndPixel[8];
extern float wavelengthToPixelFactor[8];
extern float xMinGlobalWavelength,xMaxGlobalWavelength;
extern float beginScanFrequency,endScanFrequency;

extern bool applyNewParametersSwitch;

extern QString sampleType;

extern toolBarClass *mainToolBarObj;
extern plotSpectraClass *plotSpectraObj;

extern QFont defaultFont;
extern QFont WinXFontS,WinXFontM,WinXFontL; // fonts changes with operating systems.
extern QSettings* settings;
} // namespace uiNameSpace


QT_END_NAMESPACE

class activeSpectrometers : public QWidget
{
    Q_OBJECT

    QLineEdit *sampleLineEdit;
    QLineEdit *spectrometersNameLineEdit[8];
    QLineEdit *spectrometersExposureLineEdit[8];

    QPalette paletteWhite,paletteYellow;

public:
    explicit activeSpectrometers(QWidget *parent = 0);
    ~activeSpectrometers();

signals:
    void setNewGraphsNames();

public slots:
    void setSampleType();
    void setSpectrometersName();
    void setSpectrometersExposureTime();
    void disableUnavailableSpectrometers();

private slots:
    void exportSpectrometerParamters();
    void readSpectrometerParamters();

};

#endif // ACTIVESPECTROMETERS_H
