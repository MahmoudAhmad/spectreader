#-------------------------------------------------
#
# Project created by QtCreator 2015-03-30T14:05:41
#
#-------------------------------------------------

QT       += core gui
QMAKE_CXXFLAGS += -std=c++0x\
                  -std=c++11 \
                    -std=c++1y
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpeCap1.2

TEMPLATE = app

QT += printsupport

SOURCES +=  main.cpp\
            mainwindow.cpp \
            activespectrometers.cpp \
            runparametersclass.cpp \
            filtersClass.cpp \
            plotspectraclass.cpp \
            toolBarClass.cpp \
            menubarclass.cpp \
            qcustomplot.cpp \
            spectrometerfunctions.cpp \
    specthreadclass.cpp \
    previewspectraclass.cpp

HEADERS  += mainwindow.h \
            activespectrometers.h \
            runparametersclass.h \
            filtersClass.h \
            plotspectraclass.h \
            toolBarclass.h \
            menubarclass.h \
            qcustomplot.h \
            spectrometerfunctions.h \
    ui.h \
    specthreadclass.h \
    previewspectraclass.h


win32: LIBS += -L$$PWD/ -lBWTEKUSB

INCLUDEPATH += $$PWD/
DEPENDPATH += $$PWD/

FORMS +=

RESOURCES += \
    resources.qrc
