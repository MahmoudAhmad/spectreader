#include "filtersClass.h"
#include <QCheckBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QPushButton>

filtersClass::filtersClass(QWidget *parent) : QWidget(parent)
{
    //main groupbox
    QGroupBox *filtersGroupBox=new QGroupBox("Filters ControlBox");
    filtersGroupBox->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");

    //background subtraction
    subtractBackgroundCheckBox = new QCheckBox("Subtract Background Offset.");
    connect(subtractBackgroundCheckBox,SIGNAL(toggled(bool)),this,SLOT(setSubtractBackgroundStatus(bool)));
    connect(this,SIGNAL(setRunStopButtonStatus(bool)),uiNameSpace::mainToolBarObj,SLOT(setRunStopButtonBasedonBackgourndAvailability(bool)));

    // notes included
    QCheckBox *filterNotesCheckBox = new QCheckBox("Export Data With Notes.");

    // All filters apply
    QCheckBox *allFilterMustApplyCheckBox = new QCheckBox("All Filters Must Apply.");

    filtersOuterGridLayout=new QGridLayout;
    filtersInnerGridLayout=new QGridLayout;

    filtersGroupBox->setLayout(filtersInnerGridLayout);
    filtersGroupBox->setFixedSize(200,120);
    filtersOuterGridLayout->addWidget(filtersGroupBox,0,0);


    filtersInnerGridLayout->addWidget(subtractBackgroundCheckBox,0,0,1,2);
    filtersInnerGridLayout->addWidget(filterNotesCheckBox,1,0,1,2);
    filtersInnerGridLayout->addWidget(allFilterMustApplyCheckBox,2,0,1,2);

    makeThresholdFilter();
    makeDistanceBasedFilter();
    makeOrthogonalProjectionFilter();
    makeSpectralInformationDivergenceFilter();
    setLayout(filtersOuterGridLayout);

}

filtersClass::~filtersClass()
{

}

void filtersClass::setSubtractBackgroundStatus(bool status)
{
    uiNameSpace::isBackgroundNeedToBeSubtracted=status;

    if(!uiNameSpace::isBackgroundAcquired)
    {
        emit setRunStopButtonStatus(!status);
    }
    else
    {
        emit setRunStopButtonStatus(status);
    }
}

void filtersClass::makeThresholdFilter()
{
    QGroupBox *BSF=new QGroupBox;
    BSF->setTitle("Baseline Surviving Filter");
    BSF->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");
    BSF->setCheckable(true);
    BSF->setFixedSize(195,120);

    QSlider *BSFSlider=new QSlider(Qt::Horizontal);
    BSFSlider->setRange(0,1000);
    BSFSlider->setValue(0);
    //connect(BSFSlider,SIGNAL(valueChanged(int)),this,SLOT(BSFUpdateFilterBaselineValue()));
    //connect(BSFSlider,SIGNAL(valueChanged(int)),this,SLOT(applyBSFilterToSpectra()));

    QLabel *BSFLabel=new QLabel("0");
    QCheckBox *BSFCheckBox=new QCheckBox("View baseline (Grey)");
    //connect(BSFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewBSFilterRangeOnGraph(bool)));

    QGridLayout *BSFGridLayout=new QGridLayout(BSF);
    BSFGridLayout->addWidget(BSFCheckBox,0,0,1,3);
    BSFGridLayout->addWidget(BSFSlider,1,0,1,3);
    BSFGridLayout->addWidget(BSFLabel,1,3,1,1);
    filtersOuterGridLayout->addWidget(BSF,0,1);


/*
    QCheckBox *thresholdCheckBox=new QCheckBox("Apply Threshold");
    connect(thresholdCheckBox,SIGNAL(clicked(bool)),this,SLOT(enableThresholdFilter(bool)));

    QPushButton *thresholdPushbutton=new QPushButton("Advanced");
    thresholdPushbutton->setFixedWidth(70);
    QSlider *thresholdSlider=new QSlider(Qt::Horizontal);
    QSlider *pixelsSlider=new QSlider(Qt::Horizontal);
    QLabel *thresholdLabel=new QLabel("3");
    QLabel *thresholdFixedLabel=new QLabel("above the");
    QLabel *thresholdpercentLabel=new QLabel("% line.");
    QLabel *pixelsLabel=new QLabel("90");
    QLabel *pixelFixedsLabel=new QLabel("% of pixels are");

    pixelsSlider->setMinimum(0);
    pixelsSlider->setMaximum(100);
    pixelsSlider->setValue(90);
    thresholdSlider->setMinimum(0);
    thresholdSlider->setMaximum(100);
    thresholdSlider->setValue(3);

    connect(pixelsSlider,SIGNAL(valueChanged(int)),pixelsLabel,SLOT(setNum(int)));
    connect(thresholdSlider,SIGNAL(valueChanged(int)),thresholdLabel,SLOT(setNum(int)));

    connect(pixelsSlider,SIGNAL(valueChanged(int)),this,SLOT(setThresholdFiltersNoPixels(int)));
    connect(thresholdSlider,SIGNAL(valueChanged(int)),this,SLOT(setThresholdBaseLine(int)));

    QHBoxLayout *thresholdcheckboxPushButtonHBLayout=new QHBoxLayout;
    thresholdcheckboxPushButtonHBLayout->addWidget(thresholdCheckBox);
    thresholdcheckboxPushButtonHBLayout->addWidget(thresholdPushbutton);

    QHBoxLayout *pixelsHBLayout=new QHBoxLayout;
    pixelsHBLayout->addWidget(pixelsSlider);
    pixelsHBLayout->addWidget(pixelsLabel);
    pixelsHBLayout->addWidget(pixelFixedsLabel);

    QHBoxLayout *pointsHBLayout=new QHBoxLayout;
    pointsHBLayout->addWidget(thresholdFixedLabel);
    pointsHBLayout->addWidget(thresholdLabel);
    pointsHBLayout->addWidget(thresholdpercentLabel);
    pointsHBLayout->addWidget(thresholdSlider);

    filtersInnerGridLayout->addLayout(thresholdcheckboxPushButtonHBLayout,0,3,1,1);
    filtersInnerGridLayout->addLayout(pixelsHBLayout,1,3,1,1);
    filtersInnerGridLayout->addLayout(pointsHBLayout,2,3,1,1);
    */
}

void filtersClass::enableThresholdFilter(bool thresholdFilterStatus)
{
    filters::isThresholdFilterEnabled=thresholdFilterStatus;
}

void filtersClass::setThresholdFiltersNoPixels(int noPixels)
{
    filters::thresholdFilterPercentofPixels=noPixels;qDebug()<<noPixels;
}

void filtersClass::setThresholdBaseLine(int baseLine)
{
    filters::thresholdFilterBaseLine=baseLine;
}

void filtersClass::makeDistanceBasedFilter()
{
    int noReadingInList=0;
    QGroupBox *DBF=new QGroupBox;
    DBF->setTitle("Distance-Based Filter");
    DBF->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");
    DBF->setCheckable(true);
    DBF->setFixedSize(195,120);

    QSlider *DBFSlider=new QSlider(Qt::Horizontal);
    DBFSlider->setRange(0,1000);
    DBFSlider->setValue(0);
    //connect(DBFSlider,SIGNAL(valueChanged(int)),this,SLOT(updateDBFilterThresholdAndInformation()));

    QDoubleSpinBox *DBFilterMaxValueSpinBox=new QDoubleSpinBox;
    DBFilterMaxValueSpinBox->setSingleStep(0.01);
    DBFilterMaxValueSpinBox->setValue(1.00);
    DBFilterMaxValueSpinBox->setRange(0.0,100.0);
    connect(DBFilterMaxValueSpinBox,SIGNAL(valueChanged(double)),this,SLOT(updateMaxDBFilterSliderValue(double)));

    QSlider *minDBFRangeSlider=new QSlider(Qt::Horizontal);
    QSlider *maxDBFRangeSlider=new QSlider(Qt::Horizontal);

    minDBFRangeSlider->setRange(0,noReadingInList-1);
    maxDBFRangeSlider->setRange(0,noReadingInList-1);

    minDBFRangeSlider->setValue(0);
    maxDBFRangeSlider->setValue(noReadingInList-1);
    //minDBFRangeFilter=0;
    //maxDBFRangeFilter=noReadingInList-1;

 //   connect(minDBFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(DBFUpdateFilterRange()));
 //   connect(maxDBFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(DBFUpdateFilterRange()));
 //   connect(minDBFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyDBFilterToSpectra()));
 //   connect(maxDBFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyDBFilterToSpectra()));

    QLabel *DBFLabel=new QLabel("0");

    QCheckBox *DBFCheckBox=new QCheckBox("View Range (Yellow)");
 //   connect(DBFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewDBFilterRangeOnGraph(bool)));
    QLabel *minDBFRangeLabel[2];
    QLabel *maxDBFRangeLabel[2];
    minDBFRangeLabel[0]=new QLabel("from:");
    minDBFRangeLabel[1]=new QLabel("min");
    maxDBFRangeLabel[0]=new QLabel("to:");
    maxDBFRangeLabel[1]=new QLabel("max");

    QGridLayout *BDFGridLayout=new QGridLayout(DBF);
    BDFGridLayout->addWidget(DBFSlider,0,0,1,2);
    BDFGridLayout->addWidget(DBFilterMaxValueSpinBox,0,2,1,1);
    BDFGridLayout->addWidget(DBFLabel,0,3,1,1);
    BDFGridLayout->addWidget(DBFCheckBox,1,0,1,4);
    BDFGridLayout->addWidget(minDBFRangeLabel[0],2,0,1,1);
    BDFGridLayout->addWidget(minDBFRangeLabel[1],2,1,1,1);
    BDFGridLayout->addWidget(minDBFRangeSlider,2,2,1,2);
    BDFGridLayout->addWidget(maxDBFRangeLabel[0],3,0,1,1);
    BDFGridLayout->addWidget(maxDBFRangeLabel[1],3,1,1,1);
    BDFGridLayout->addWidget(maxDBFRangeSlider,3,2,1,2);
    filtersOuterGridLayout->addWidget(DBF,0,2);

    /*
    QHBoxLayout *DBFHBLayout=new QHBoxLayout;
    QCheckBox *DBFCheckBox=new QCheckBox("Distance-Based Filter");
    QPushButton *DBFPushbutton=new QPushButton;
    DBFPushbutton->setText("Advanced");
    DBFPushbutton->setFixedWidth(70);

    DBFHBLayout->addWidget(DBFCheckBox);
    DBFHBLayout->addWidget(DBFPushbutton);
    filtersInnerGridLayout->addLayout(DBFHBLayout,0,4,1,1);
    */
}

void filtersClass::makeOrthogonalProjectionFilter()
{
    int noReadingInList=0;
    QGroupBox *OPF=new QGroupBox;
    OPF->setTitle("Ortho-Projection Filter");
    OPF->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");
    OPF->setCheckable(true);
    OPF->setFixedSize(195,120);

    QSlider *OPFSlider=new QSlider(Qt::Horizontal);
    OPFSlider->setRange(0,1000);
    OPFSlider->setValue(0);
    //connect(OPFSlider,SIGNAL(valueChanged(int)),this,SLOT(updateOPFilterSurvivors()));

    QDoubleSpinBox *OPFilterMaxValueSpinBox=new QDoubleSpinBox;
    OPFilterMaxValueSpinBox->setSingleStep(0.01);
    OPFilterMaxValueSpinBox->setValue(1.00);
    OPFilterMaxValueSpinBox->setRange(0.0,100.0);
    //connect(OPFilterMaxValueSpinBox,SIGNAL(valueChanged(double)),this,SLOT(updateMaxOPFilterSliderValue(double)));

    QSlider *minOPFRangeSlider=new QSlider(Qt::Horizontal);
    QSlider *maxOPFRangeSlider=new QSlider(Qt::Horizontal);

    minOPFRangeSlider->setRange(0,noReadingInList-1);
    maxOPFRangeSlider->setRange(0,noReadingInList-1);

    minOPFRangeSlider->setValue(0);
    maxOPFRangeSlider->setValue(noReadingInList-1);
    //minOPFRangeFilter=0;
    //maxOPFRangeFilter=noReadingInList-1;

    //connect(minOPFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(OPFUpdateFilterRange()));
    //connect(maxOPFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(OPFUpdateFilterRange()));
    //connect(minOPFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyOPFilterToSpectra()));
    //connect(maxOPFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyOPFilterToSpectra()));

    QLabel *OPFLabel=new QLabel("0");

    QCheckBox *OPFCheckBox=new QCheckBox("View Range (Green)");
    //connect(OPFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewOPFilterRangeOnGraph(bool)));
    QLabel *minOPFRangeLabel[2];
    QLabel *maxOPFRangeLabel[2];

    minOPFRangeLabel[0]=new QLabel("from:");
    minOPFRangeLabel[1]=new QLabel("min");
    maxOPFRangeLabel[0]=new QLabel("to:");
    maxOPFRangeLabel[1]=new QLabel("max");

    QGridLayout *OPFGridLayout=new QGridLayout(OPF);
    OPFGridLayout->addWidget(OPFSlider,0,0,1,2);
    OPFGridLayout->addWidget(OPFilterMaxValueSpinBox,0,2,1,1);
    OPFGridLayout->addWidget(OPFLabel,0,3,1,1);
    OPFGridLayout->addWidget(OPFCheckBox,1,0,1,4);
    OPFGridLayout->addWidget(minOPFRangeLabel[0],2,0,1,1);
    OPFGridLayout->addWidget(minOPFRangeLabel[1],2,1,1,1);
    OPFGridLayout->addWidget(minOPFRangeSlider,2,2,1,2);
    OPFGridLayout->addWidget(maxOPFRangeLabel[0],3,0,1,1);
    OPFGridLayout->addWidget(maxOPFRangeLabel[1],3,1,1,1);
    OPFGridLayout->addWidget(maxOPFRangeSlider,3,2,1,2);
    filtersOuterGridLayout->addWidget(OPF,0,3);


    /*
    QHBoxLayout *OPHBLayout=new QHBoxLayout;
    QCheckBox *OPFCheckBox=new QCheckBox("Ortho-Projection Filter");
    QPushButton *OPFPushbutton=new QPushButton;
    OPFPushbutton->setText("Advanced");
    OPFPushbutton->setFixedWidth(70);

    OPHBLayout->addWidget(OPFCheckBox);
    OPHBLayout->addWidget(OPFPushbutton);
    filtersInnerGridLayout->addLayout(OPHBLayout,0,5,1,1);
*/
}

void filtersClass::makeSpectralInformationDivergenceFilter()
{
    int noReadingInList=0;
    QGroupBox *SIDF=new QGroupBox;
    SIDF->setTitle("Spectral Info. Div Filter");
    SIDF->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");
    SIDF->setCheckable(true);
    SIDF->setFixedSize(195,120);

    QSlider *SIDFSlider=new QSlider(Qt::Horizontal);
    SIDFSlider->setRange(0,1000);
    SIDFSlider->setValue(0);
    //connect(SIDFSlider,SIGNAL(valueChanged(int)),this,SLOT(updateSIDFilterSurvivors()));

    QDoubleSpinBox *SIDFilterMaxValueSpinBox=new QDoubleSpinBox;
    SIDFilterMaxValueSpinBox->setSingleStep(0.01);
    SIDFilterMaxValueSpinBox->setValue(1.00);
    SIDFilterMaxValueSpinBox->setRange(0.0,100.0);
    //connect(SIDFilterMaxValueSpinBox,SIGNAL(valueChanged(double)),this,SLOT(updateMaxSIDFilterSliderValue(double)));

    QSlider *minSIDFRangeSlider=new QSlider(Qt::Horizontal);
    QSlider *maxSIDFRangeSlider=new QSlider(Qt::Horizontal);

    minSIDFRangeSlider->setRange(0,noReadingInList-1);
    maxSIDFRangeSlider->setRange(0,noReadingInList-1);

    minSIDFRangeSlider->setValue(0);
    maxSIDFRangeSlider->setValue(noReadingInList-1);
    //minSIDFRangeFilter=0;
    //maxSIDFRangeFilter=noReadingInList-1;

    //connect(minSIDFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(SIDFUpdateFilterRange()));
    //connect(maxSIDFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(SIDFUpdateFilterRange()));
    //connect(minSIDFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applySIDFilterToSpectra()));
    //connect(maxSIDFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applySIDFilterToSpectra()));

    QLabel *SIDFLabel=new QLabel("0");

    QCheckBox *SIDFCheckBox=new QCheckBox("View Range (Blue)");
    //connect(SIDFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewSIDFilterRangeOnGraph(bool)));
    QLabel *minSIDFRangeLabel[2];
    QLabel *maxSIDFRangeLabel[2];


    minSIDFRangeLabel[0]=new QLabel("from:");
    minSIDFRangeLabel[1]=new QLabel("min");
    maxSIDFRangeLabel[0]=new QLabel("to:");
    maxSIDFRangeLabel[1]=new QLabel("max");

    QGridLayout *SIDFGridLayout=new QGridLayout(SIDF);
    SIDFGridLayout->addWidget(SIDFSlider,0,0,1,2);
    SIDFGridLayout->addWidget(SIDFilterMaxValueSpinBox,0,2,1,1);
    SIDFGridLayout->addWidget(SIDFLabel,0,3,1,1);
    SIDFGridLayout->addWidget(SIDFCheckBox,1,0,1,4);
    SIDFGridLayout->addWidget(minSIDFRangeLabel[0],2,0,1,1);
    SIDFGridLayout->addWidget(minSIDFRangeLabel[1],2,1,1,1);
    SIDFGridLayout->addWidget(minSIDFRangeSlider,2,2,1,2);
    SIDFGridLayout->addWidget(maxSIDFRangeLabel[0],3,0,1,1);
    SIDFGridLayout->addWidget(maxSIDFRangeLabel[1],3,1,1,1);
    SIDFGridLayout->addWidget(maxSIDFRangeSlider,3,2,1,2);
    filtersOuterGridLayout->addWidget(SIDF,0,4);

    /*
    QHBoxLayout *SIDHBLayout=new QHBoxLayout;
    QCheckBox *SIDCheckBox=new QCheckBox("Spectral Info. Div. Filter");
    QPushButton *SIDPushbutton=new QPushButton;
    SIDPushbutton->setText("Advanced");
    SIDPushbutton->setFixedWidth(70 );

    SIDHBLayout->addWidget(SIDCheckBox);
    SIDHBLayout->addWidget(SIDPushbutton);
    filtersInnerGridLayout->addLayout(SIDHBLayout,0,6,1,1);
    */
}
