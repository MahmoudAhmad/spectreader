#include "toolBarclass.h"
#include "specthreadclass.h"
#include "spectrometerfunctions.h"
#include "previewspectraclass.h"
#include <QIcon>
#include <QPixmap>
#include <QSize>
#include <QPalette>
#include <QToolButton>
#include <QtCore>
#include <QDesktopServices>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QFileInfo>
#include <QWidget>

toolBarClass::toolBarClass(QWidget *parent) : QToolBar(parent)
{
    if(uiNameSpace::noSpectrometers==0)
        enableDataButtons();

// set directory button
    setDirectoryButton->setIcon(QIcon(":/icons/resources/setOutputDir.png"));
    QAction *setOutputDirAction=addWidget(setDirectoryButton);
    this->addAction(setOutputDirAction);
    connect(setDirectoryButton,SIGNAL(clicked()),this,SLOT(setOutputDirectoryFunction()));

// set directory button
    openOutputFolderButton->setIcon(QIcon(":/icons/resources/openOutputDir.png"));
    QAction *openOutputDirAction=addWidget(openOutputFolderButton);
    this->addAction(openOutputDirAction);
    connect(openOutputFolderButton,SIGNAL(clicked()),this,SLOT(openOutputFilesFunction()));

    this->addSeparator();

// reload devices button
    reloadDevicesButton->setIcon(QIcon(":/icons/resources/refreshDevices.png"));
    QAction *reloadDevicesAction=addWidget(reloadDevicesButton);
    this->addAction(reloadDevicesAction);
    connect(reloadDevicesButton,SIGNAL(released()),this,SLOT(reInitiateSpectrometers()));

// save run settings
   saveSettingsButton->setIcon(QIcon(":/icons/resources/saveSettings.png"));
   QAction *saveSettingsAction=addWidget(saveSettingsButton);
   this->addAction(saveSettingsAction);
   connect(saveSettingsButton,SIGNAL(released()),this,SLOT(saveSettingsFunction()));

// load run settings
   loadSettingsButton->setIcon(QIcon(":/icons/resources/loadSettings.png"));
   QAction *loadSettingsAction=addWidget(loadSettingsButton);
   this->addAction(loadSettingsAction);
   connect(loadSettingsButton,SIGNAL(released()),this,SLOT(loadSettingsFunction()));


   this->addSeparator();

//run and stop buttons
    runStopToolButton->setIcon(QIcon(":/icons/resources/run.png"));
    QAction *runStopAction=addWidget(runStopToolButton);
    this->addAction(runStopAction);
    connect(runStopToolButton,SIGNAL(released()),this,SLOT(acquiringDataNow()));


//run once
    acquireLightOnceButton->setIcon(QIcon(":/icons/resources/runOnceOn.png"));
    QAction *acquireLightOnceAction=addWidget(acquireLightOnceButton);
    this->addAction(acquireLightOnceAction);
    connect(acquireLightOnceButton,SIGNAL(released()),this,SLOT(acquireLightOnce()));

//acquire background
    acquireBackgroundButton->setIcon(QIcon(":/icons/resources/dark.png"));
    QAction *acquireBackgroundAction=addWidget(acquireBackgroundButton);
    this->addAction(acquireBackgroundAction);
    connect(acquireBackgroundButton,SIGNAL(released()),this,SLOT(acquireDarkSignalNow()));

    this->addSeparator();

//open spectra file Button
        previewOldDataButton->setIcon(QIcon(":/icons/resources/preview.png"));
        QAction *previewOldDataAction=addWidget(previewOldDataButton);
        this->addAction(previewOldDataAction);
        this->setFloatable(false);
        this->setMovable(false);
        this->setIconSize(QSize(40,40));
        connect(previewOldDataButton,SIGNAL(released()),this,SLOT(previewSpectraFromFile()));


// filters
         GenerateFiltersButton->setIcon(QIcon(":/icons/resources/saveFilter.png"));
         QAction *GenerateFiltersAction=addWidget(GenerateFiltersButton);
         this->addAction(GenerateFiltersAction);
         this->setFloatable(false);
         this->setMovable(false);
         this->setIconSize(QSize(40,40));


        this->addSeparator();

//information button
    informationButton->setIcon(QIcon(":/icons/resources/information.png"));
    QAction *informationAction=addWidget(informationButton);
    this->addAction(informationAction);

    this->setFloatable(false);
    this->setMovable(false);
    this->setIconSize(QSize(40,40));


// save setting dialogue:
    QDir settingsDir(QApplication::applicationDirPath()+"/settings templates/");
    if (!settingsDir.exists())
        settingsDir.mkdir(QApplication::applicationDirPath()+"/settings templates/");

    uiNameSpace::settingsFileDir=settingsDir.path();

} // end constructor

toolBarClass::~toolBarClass()
{

}

bool toolBarClass::checkForTimingParametersAndRunIfSucceed()
{
    //test all parameters for porper values
    if(uiNameSpace::isModeMean)
    {
        for(int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            if(uiNameSpace::spectrometerExposureInmSec[i]*uiNameSpace::scansAveraged >= uiNameSpace::readingIntervalInmSec)
            {
                QMessageBox::information(this,"Error","Reading intervals must be > maximum exposure time x averaged scans. Fix timing and run again.");
                return false;
            }
        }
    }
    else if (!uiNameSpace::isModeMean)
    {
        for(int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            if(uiNameSpace::spectrometerExposureInmSec[i]*uiNameSpace::scansAveraged*3 >= uiNameSpace::readingIntervalInmSec)
            {
                QMessageBox::information(this,"Error","Reading intervals must be > maximum exposure time x averaged scans. Fix timing and run again.");
                return false;
            }
        }
    }

    if(uiNameSpace::readingIntervalInmSec > uiNameSpace::totalRunTimeInmSec)
    {
        QMessageBox::information(this,"Error","Reading intervals must be < total run time. Fix timing and run again.");
        return false;
    }

        return true;
}

bool toolBarClass::acquiringDataNow()
{
    if (isRunStopButtonClicked and checkForTimingParametersAndRunIfSucceed())
    {
        uiNameSpace::isAcquiringLightNow=false;
        this->runStopToolButton->setIcon(QIcon(":/icons/resources/run.png"));
        this->acquireBackgroundButton->setEnabled(true);
        acquireLightOnceButton->setDisabled(false);
        acquireBackgroundButton->setEnabled(true);
        isRunStopButtonClicked=false;
        stopAcquiringStoringPlottingThreads();
    }
    else if (checkForTimingParametersAndRunIfSucceed())
    {
        progressCounter=0;
        uiNameSpace::isAcquiringLightNow=true;
        this->runStopToolButton->setIcon(QIcon(":/icons/resources/stop.png"));
        this->acquireBackgroundButton->setEnabled(false);
        acquireBackgroundButton->setEnabled(false);
        acquireLightOnceButton->setDisabled(true);
        isRunStopButtonClicked=true;
        acquireingStoringPlottingIsRunning();
        for(int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            uiNameSpace::printDataHeadings[i]=true;
        }
    }

return true;
}

void toolBarClass::acquireLightOnce()
{
    if (isRunOnceButtonClicked)
    {
        this->acquireLightOnceButton->setIcon(QIcon(":/icons/resources/runOnceOn.png"));
        runStopToolButton->setEnabled(true);
        acquireBackgroundButton->setEnabled(true);
        uiNameSpace::acquireLightOnceNow=false;
        isRunOnceButtonClicked=!isRunOnceButtonClicked;
        stopAcquiringStoringPlottingThreads();
    }
    else if (!isRunOnceButtonClicked)
    {
        progressCounter=0;
        runStopToolButton->setEnabled(false);
        acquireBackgroundButton->setEnabled(false);
        this->acquireLightOnceButton->setIcon(QIcon(":/icons/resources/runOnceOff.png"));
        isRunOnceButtonClicked=!isRunOnceButtonClicked;
        uiNameSpace::acquireLightOnceNow=true;
        for(int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            uiNameSpace::printDataHeadings[i]=true;
        }
        acquireingStoringPlottingIsRunning();
    }
}

bool toolBarClass::acquireingStoringPlottingIsRunning()
{

    for (int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
         spectThreads[i]=new specThreadClass;
         for(int j=0;j<uiNameSpace::noSpectrometers;j++)
         {
             if(uiNameSpace::preferedSpectsOrder[j]==uiNameSpace::spectC_Code[i])
             {
                 spectThreads[i]->setSpectrometerIndex(i,j);
                 connect(spectThreads[i],SIGNAL(plotNewData(int,int)),uiNameSpace::plotSpectraObj,SLOT(plotRecievedData(int,int)));
                 connect(spectThreads[i],SIGNAL(updateProgressBar()),this,SLOT(updateProgress()));
             }
         }

         spectThreads[i]->start();
    }
    return true;
}

void toolBarClass::stopAcquiringStoringPlottingThreads()
{
    for (int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
         spectThreads[i]->quitThreadNow();
    }
}

void toolBarClass::acquireDarkSignalNow()
{
    if (isAcquireDarkSignalClicked) //first is Flase !
    {
        this->acquireBackgroundButton->setIcon(QIcon(":/icons/resources/dark.png"));
        this->runStopToolButton->setEnabled(true);
        this->reloadDevicesButton->setEnabled(true);
        acquireLightOnceButton->setDisabled(false);
        isAcquireDarkSignalClicked=false;
        uiNameSpace::isBackgroundAcquired=true;
        uiNameSpace::isAcquiringDarkSignalNow=false;
        stopAcquiringStoringPlottingThreads();
    }
    else if (!isAcquireDarkSignalClicked)
    {
        progressCounter=0;
        this->acquireBackgroundButton->setIcon(QIcon(":/icons/resources/stopDark.png"));
        this->runStopToolButton->setDisabled(true);
        this->reloadDevicesButton->setDisabled(true);
        acquireLightOnceButton->setDisabled(true);
        isAcquireDarkSignalClicked=true;
        uiNameSpace::isAcquiringDarkSignalNow=true;
        uiNameSpace::isAcquiringLightNow=false;
        acquireingStoringPlottingIsRunning();
    }
}

void toolBarClass::updateProgress()
{
    progressBarLock.lock();
    if(uiNameSpace::isAcquiringLightNow)
    {
        progressCounter++;
        uiNameSpace::mainProgressBarObj->setValue(100*progressCounter/(uiNameSpace::totalRunTimeInmSec/uiNameSpace::readingIntervalInmSec*uiNameSpace::noSpectrometers));
        uiNameSpace::mainStatusBarObj->showMessage(QString::number(progressCounter)+" readings acquired out of "+QString::number(uiNameSpace::totalRunTimeInmSec/uiNameSpace::readingIntervalInmSec*uiNameSpace::noSpectrometers));

    }
    else if(uiNameSpace::isAcquiringDarkSignalNow)
    {
        progressCounter++;
        uiNameSpace::mainProgressBarObj->setValue(100*progressCounter/uiNameSpace::noSpectrometers);
        uiNameSpace::mainStatusBarObj->showMessage(QString::number(progressCounter)+" readings acquired out of "+QString::number(uiNameSpace::noSpectrometers));

    }
    else if (uiNameSpace::acquireLightOnceNow)
    {
        progressCounter++;
        uiNameSpace::mainProgressBarObj->setValue(100*progressCounter/uiNameSpace::noSpectrometers);
        uiNameSpace::mainStatusBarObj->showMessage(QString::number(progressCounter)+" readings acquired out of "+QString::number(uiNameSpace::noSpectrometers));
    }
    progressBarLock.unlock();


}

void toolBarClass::reInitiateSpectrometers()
{
    uiNameSpace::shutdownSpectrometers();
    bool isAccomplished = uiNameSpace::initiateSpectrometers();
    if (!isAccomplished)
    {
       uiNameSpace::mainStatusBarObj->showMessage("Problem with devices initiations",3600000);
       uiNameSpace::noSpectrometers=6;

      // return;
    }

    enableDataButtons();

    emit updateActiveSpects();
}

void toolBarClass::setRunStopButtonBasedonBackgourndAvailability(bool status)
{
    if(!uiNameSpace::isBackgroundAcquired)
        runStopToolButton->setEnabled(status);
}

void toolBarClass::setOutputDirectoryFunction()
{
    setDirectoryQString=QFileDialog::getExistingDirectory(this, tr("Open Directory"),uiNameSpace::outputFilesDir,QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);
    uiNameSpace::outputFilesDir=setDirectoryQString;
}

void toolBarClass::openOutputFilesFunction()
{
    QDesktopServices::openUrl(QUrl("file:///"+uiNameSpace::outputFilesDir, QUrl::TolerantMode));
}

void toolBarClass::progressBaris100(int progressPercent)
{
    if(progressPercent >= 100 and uiNameSpace::isAcquiringDarkSignalNow)
    {
        acquireDarkSignalNow();
    }
    else if (progressPercent >= 100 and isRunStopButtonClicked)
    {
        acquiringDataNow();
    }
    else if (progressPercent >= 100 and acquireLightOnceButton)
    {
        acquireLightOnce();
    }

}

void toolBarClass::enableDataButtons()
{
    if(uiNameSpace::noSpectrometers==0)
    {
        runStopToolButton->setDisabled(true);
        acquireLightOnceButton->setDisabled(true);
        acquireBackgroundButton->setDisabled(true);
    }
    else
    {
        runStopToolButton->setEnabled(true);
        acquireLightOnceButton->setEnabled(true);
        acquireBackgroundButton->setEnabled(true);
    }
}

void toolBarClass::saveSettingsFunction()
{
    uiNameSpace::settingsFileName=QFileDialog::getSaveFileName(this,"Settings File Name",uiNameSpace::settingsFileDir,tr("*.ini"));
    uiNameSpace::settingsFileName.append(".ini");
    uiNameSpace::settingsFileDir=QFileInfo(uiNameSpace::settingsFileName).path();

    uiNameSpace::settings = new QSettings(uiNameSpace::settingsFileName, QSettings::IniFormat);
    uiNameSpace::settings->setIniCodec("UTF-8");

    emit exportRunParameters();
    emit exportSpectrometerParamters();
}

void toolBarClass::loadSettingsFunction()
{

    uiNameSpace::settingsFileName=QFileDialog::getOpenFileName(this,"Settings File Name",uiNameSpace::settingsFileDir,tr("*.ini"));
    uiNameSpace::settingsFileDir=QFileInfo(uiNameSpace::settingsFileName).path();

    uiNameSpace::settings = new QSettings(uiNameSpace::settingsFileName, QSettings::IniFormat);
    uiNameSpace::settings->setIniCodec("UTF-8");

    emit readRunParameters();
    emit readSpectrometerParamters();

}

void toolBarClass::previewSpectraFromFile()
{
    // select file to preview
    QString spectraFileName = QFileDialog::getOpenFileName(this,tr("Open File"),uiNameSpace::outputFilesDir, tr("Data Files (*.Dat *.out)"));

    // create the window and the items
    previewSpectraClass *previewSpectraObj=new previewSpectraClass(0,spectraFileName);
    previewSpectraObj->show();

}
