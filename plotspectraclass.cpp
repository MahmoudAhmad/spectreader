#include "plotspectraclass.h"
#include "qcustomplot.h"
#include <QVector>
#include <QDebug>
#include <QSlider>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QWidget>

plotSpectraClass::plotSpectraClass(QWidget *parent) : QWidget(parent)
{
    PlotArea=new QCustomPlot;
    plotsTabWidget=new QTabWidget;
    plotsTabWidget->addTab(PlotArea,"Instantaneous Spectra");

    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        PlotsHideButton[i]=new QPushButton("Hide Graph");
        PlotsDeleteButton[i]=new QPushButton("Delete from File");
        plotsSlider[i]=new QSlider(Qt::Horizontal);
        plotsSliderLabel[i]=new QLabel("Graph#0");

        solePlots[i]=new QCustomPlot;
        plotsHBoxLayout[i]=new QHBoxLayout;
        PlotsVBLayout[i]= new QVBoxLayout;
        plotsWidgetforLayouts[i]=new QWidget;
        plotsHBoxLayout[i]->addWidget(plotsSlider[i]);
        plotsHBoxLayout[i]->addWidget(plotsSliderLabel[i]);
        plotsHBoxLayout[i]->addWidget(PlotsHideButton[i]);
        plotsHBoxLayout[i]->addWidget(PlotsDeleteButton[i]);
        PlotsVBLayout[i]->addWidget(solePlots[i]);
        PlotsVBLayout[i]->addLayout(plotsHBoxLayout[i]);
        plotsWidgetforLayouts[i]->setLayout(PlotsVBLayout[i]);
        plotsTabWidget->addTab(plotsWidgetforLayouts[i],"Ch"+QString::number(i));
        connect(plotsSlider[i],SIGNAL(valueChanged(int)),this,SLOT(updateFigsInTabs(int)));
        connect(PlotsHideButton[i],SIGNAL(clicked()),this,SLOT(hideGraphsInTimeLine()));
        connect(PlotsDeleteButton[i],SIGNAL(clicked()),this,SLOT(deleteGraph()));
        plotsSlider[i]->setTickInterval(1);
        plotsSlider[i]->setMinimum(0);
        plotsSlider[i]->setMaximum(0);
    }

    QGroupBox *plotSpectraGroupBox=new QGroupBox("Active Spectra");
    plotSpectraGroupBox->setFont(uiNameSpace::WinXFontM);
    plotSpectraGroupBox->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");
    QGridLayout *plotLayout=new QGridLayout;

    plotLayout->addWidget(plotsTabWidget);

    plotSpectraGroupBox->setLayout(plotLayout);
    QGridLayout *plotSpectraLayout = new QGridLayout;
    plotSpectraLayout->addWidget(plotSpectraGroupBox);
    plotSpectraGroupBox->setFixedSize(1000,430);
    setLayout(plotSpectraLayout);

    this->startPlotting();
}

plotSpectraClass::~plotSpectraClass()
{

}

void plotSpectraClass::startPlotting()
{

        pen[0].setColor(QColor(255,0,0));
        brush[0]=QBrush(QColor(255,30,20,20));

        pen[1].setColor(QColor(0,255,0));
        brush[1]=QBrush(QColor(30,255,30,20));

        pen[2].setColor(QColor(0,0,255));
        brush[2]=QBrush(QColor(30,30,255,20));

        pen[3].setColor(QColor(255,255,0));
        brush[3]=QBrush(QColor(255,255,30,20));

        pen[4].setColor(QColor(0,255,255));
        brush[4]=QBrush(QColor(30,255,255,20));

        pen[5].setColor(QColor(255,0,255));
        brush[5]=QBrush(QColor(255,30,255,20));

        pen[6].setColor(QColor(128,0,0));
        brush[6]=QBrush(QColor(128,15,15,20));

        pen[7].setColor(QColor(128,128,0));
        brush[7]=QBrush(QColor(128,128,15,20));

        for (int i=0;i<uiNameSpace::noSpectrometers;i++)
        {
            PlotArea->addGraph();
            pen[i].setStyle(Qt::SolidLine);
            pen[i].setWidth(1);
            PlotArea->graph(i)->setPen(pen[i]);
            PlotArea->graph(i)->setBrush(brush[i]);
            PlotArea->graph(i)->setName("Ch"+QString::number(i));

            solePlots[i]->legend->setVisible(false);
            solePlots[i]->xAxis->setLabel("Wavelength (nm)");
            solePlots[i]->yAxis->setLabel("Absolute Intensity (Arb. Units)");
            solePlots[i]->xAxis->setRange(300,900);
            solePlots[i]->yAxis->setRange(0,1.000001);
            solePlots[i]->xAxis->setAutoTickCount(9);
            solePlots[i]->yAxis->setAutoTickCount(5);
            solePlots[i]->setInteraction(QCP::iRangeDrag,true);
            solePlots[i]->setInteraction(QCP::iRangeZoom,true);
            solePlots[i]->setInteraction(QCP::iSelectAxes,true);
            solePlots[i]->axisRect()->setupFullAxesBox();
        }

      PlotArea->legend->setVisible(true);
      PlotArea->legend->setFont(QFont("Helvetica",9));
      PlotArea->xAxis->setLabel("Wavelength (nm)");
      PlotArea->yAxis->setLabel("Absolute Intensity (Arb. Units)");
      PlotArea->xAxis->setRange(300,900);
      PlotArea->yAxis->setRange(0,1.000001);
      PlotArea->yAxis->setAutoTickCount(5);
      PlotArea->xAxis->setAutoTickCount(9);

      PlotArea->setInteraction(QCP::iRangeDrag,true);
      PlotArea->setInteraction(QCP::iRangeZoom,true);
      PlotArea->setInteraction(QCP::iSelectAxes,true);

      PlotArea->rescaleAxes(true);
      PlotArea->axisRect()->setupFullAxesBox();
      // set locale to english, so we get english decimal separator:
      PlotArea->setLocale(QLocale(QLocale::English, QLocale::UnitedKingdom));
}

void plotSpectraClass::plotRecievedData(int currentChannel,int spectrometerIndex)
{
    fCoefsA[0]=uiNameSpace::fcoeffA[currentChannel][0];
    fCoefsA[1]=uiNameSpace::fcoeffA[currentChannel][1];
    fCoefsA[2]=uiNameSpace::fcoeffA[currentChannel][2];
    fCoefsA[3]=uiNameSpace::fcoeffA[currentChannel][3];
    fCoefsB[0]=uiNameSpace::fcoeffB[currentChannel][0];
    fCoefsB[1]=uiNameSpace::fcoeffB[currentChannel][1];
    fCoefsB[2]=uiNameSpace::fcoeffB[currentChannel][2];
    fCoefsB[3]=uiNameSpace::fcoeffB[currentChannel][3];

    QVector<double> xData(2048), yData(2048);
    for (int i=uiNameSpace::xBeginPixel[currentChannel]; i<uiNameSpace::xEndPixel[currentChannel]+1; i++)
    {
        xData[i]=fCoefsA[0]+fCoefsA[1]*i+fCoefsA[2]*qPow(i,2)+fCoefsA[3]*qPow(i,3);
        yData[i]=(double)uiNameSpace::spectOutput[spectrometerIndex][i]/65535.0;
    }

    PlotArea->graph(spectrometerIndex)->setData(xData,yData);
    PlotArea->replot();

// plot tabs graphs

    solePlots[spectrometerIndex]->addGraph();
    solePlots[spectrometerIndex]->graph(graphIndex[spectrometerIndex])->setData(xData,yData);
    if(plotsTabWidget->currentIndex()-1!=spectrometerIndex)
    plotsSlider[spectrometerIndex]->setMaximum(graphIndex[spectrometerIndex]);

    solePlots[spectrometerIndex]->graph(graphIndex[spectrometerIndex])->setPen(pen[spectrometerIndex]);
    solePlots[spectrometerIndex]->graph(graphIndex[spectrometerIndex])->setVisible(false);
    xMinAxis[spectrometerIndex].append(uiNameSpace::beginScanFrequency);
    xMaxAxis[spectrometerIndex].append(uiNameSpace::endScanFrequency);
    isGraphHidden[spectrometerIndex].append(false);
    graphIndex[spectrometerIndex]++;

}

void plotSpectraClass::setNewFrequencyRange()
{
    PlotArea->xAxis->setRange(uiNameSpace::beginScanFrequency,uiNameSpace::endScanFrequency);
    PlotArea->replot();

    for (int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        solePlots[i]->xAxis->setRange(uiNameSpace::beginScanFrequency,uiNameSpace::endScanFrequency);
        solePlots[i]->replot();
    }
}

void plotSpectraClass::setNewIntensityRange()
{
    PlotArea->yAxis->setRange(uiNameSpace::yMinValue,uiNameSpace::yMaxValue);
    PlotArea->replot();

    for (int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        solePlots[i]->yAxis->setRange(uiNameSpace::yMinValue,uiNameSpace::yMaxValue);
        solePlots[i]->replot();
    }

}

void plotSpectraClass::setNewGraphsNames()
{
    for (int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        PlotArea->graph(i)->setName(uiNameSpace::spectrometerName[i]);
        plotsTabWidget->setTabText(i+1,uiNameSpace::spectrometerName[i]); // first one for all of them
    }
    PlotArea->replot();
}

void plotSpectraClass::hideLegend(bool isVisibleLegend)
{
    PlotArea->legend->setVisible(isVisibleLegend);
    PlotArea->replot();
}

void plotSpectraClass::updateFigsInTabs(int index)
{
    if(plotsTabWidget->currentIndex()==0)
        return;

    solePlots[plotsTabWidget->currentIndex()-1]->graph(lastPlotIndex[plotsTabWidget->currentIndex()-1])->setVisible(false);
    if(!isGraphHidden[plotsTabWidget->currentIndex()-1][index])
        solePlots[plotsTabWidget->currentIndex()-1]->graph(index)->setVisible(true);

    solePlots[plotsTabWidget->currentIndex()-1]->xAxis->setRange(xMinAxis[plotsTabWidget->currentIndex()-1][index],xMaxAxis[plotsTabWidget->currentIndex()-1][index]);
    solePlots[plotsTabWidget->currentIndex()-1]->replot();
    plotsSliderLabel[plotsTabWidget->currentIndex()-1]->setText("Graph#"+QString::number(index)+", acquired at:"+uiNameSpace::graphsTimeStamp[plotsTabWidget->currentIndex()-1][index]);
    lastPlotIndex[plotsTabWidget->currentIndex()-1]=index;

    if(isGraphHidden[plotsTabWidget->currentIndex()-1][index])
        PlotsHideButton[plotsTabWidget->currentIndex()-1]->setText("Show Graph");
    else
        PlotsHideButton[plotsTabWidget->currentIndex()-1]->setText("Hide Graph");

}

void plotSpectraClass::deleteGraph()
{
    if(graphIndex[plotsTabWidget->currentIndex()-1]==0)
        return;

    PlotsDeleteButton[plotsTabWidget->currentIndex()-1]->setDisabled(true);
    solePlots[plotsTabWidget->currentIndex()-1]->graph(plotsSlider[plotsTabWidget->currentIndex()-1]->value())->clearData();
    solePlots[plotsTabWidget->currentIndex()-1]->replot();

    oldLine[plotsTabWidget->currentIndex()-1].clear();
    newLines[plotsTabWidget->currentIndex()-1].clear();

    tmpFileToDeleteData[plotsTabWidget->currentIndex()-1].setFileName(uiNameSpace::oldFileNames[plotsTabWidget->currentIndex()-1]);
    if (!tmpFileToDeleteData[plotsTabWidget->currentIndex()-1].open(QFile::ReadWrite | QFile::Text))
        QMessageBox::information(this,"Error","File not Found for channel #"+QString::number(plotsTabWidget->currentIndex()-1)+".");

    tmpText[plotsTabWidget->currentIndex()-1].setDevice(&tmpFileToDeleteData[plotsTabWidget->currentIndex()-1]);

    while(!tmpText[plotsTabWidget->currentIndex()-1].atEnd())
    {
        oldLine[plotsTabWidget->currentIndex()-1]=tmpText[plotsTabWidget->currentIndex()-1].readLine();
        if(!oldLine[plotsTabWidget->currentIndex()-1].contains(uiNameSpace::graphsTimeStamp[plotsTabWidget->currentIndex()-1][plotsSlider[plotsTabWidget->currentIndex()-1]->value()]))
            newLines[plotsTabWidget->currentIndex()-1].append(oldLine[plotsTabWidget->currentIndex()-1] + "\n");
    }

    tmpFileToDeleteData[plotsTabWidget->currentIndex()-1].resize(0);
    tmpText[plotsTabWidget->currentIndex()-1] <<newLines[plotsTabWidget->currentIndex()-1];
    tmpFileToDeleteData[plotsTabWidget->currentIndex()-1].close();
    PlotsDeleteButton[plotsTabWidget->currentIndex()-1]->setEnabled(true);

}

void plotSpectraClass::hideGraphsInTimeLine()
{
    if(graphIndex[plotsTabWidget->currentIndex()-1]==0)
        return;

    if (isGraphHidden[plotsTabWidget->currentIndex()-1][plotsSlider[plotsTabWidget->currentIndex()-1]->value()])
    {
        isGraphHidden[plotsTabWidget->currentIndex()-1][plotsSlider[plotsTabWidget->currentIndex()-1]->value()]=false;
        solePlots[plotsTabWidget->currentIndex()-1]->graph(plotsSlider[plotsTabWidget->currentIndex()-1]->value())->setVisible(true);
        solePlots[plotsTabWidget->currentIndex()-1]->replot();
        PlotsHideButton[plotsTabWidget->currentIndex()-1]->setText("Hide Graph");
    }
    else
    {
        isGraphHidden[plotsTabWidget->currentIndex()-1][plotsSlider[plotsTabWidget->currentIndex()-1]->value()]=true;
        solePlots[plotsTabWidget->currentIndex()-1]->graph(plotsSlider[plotsTabWidget->currentIndex()-1]->value())->setVisible(false);
        solePlots[plotsTabWidget->currentIndex()-1]->replot();
        PlotsHideButton[plotsTabWidget->currentIndex()-1]->setText("Show Graph");
    }

}

void plotSpectraClass::resetPlotParameters(int spectrometersIndex)
{
        plotsSlider[spectrometersIndex]->setValue(0);
        plotsSlider[spectrometersIndex]->setMaximum(0);
        solePlots[spectrometersIndex]->clearGraphs();
        graphIndex[spectrometersIndex]=0;
        xMinAxis[spectrometersIndex].clear();
        xMaxAxis[spectrometersIndex].clear();
        isGraphHidden[spectrometersIndex].clear();
        lastPlotIndex[spectrometersIndex]=0;
}
