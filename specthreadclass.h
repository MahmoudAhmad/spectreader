#ifndef SPECTHREADCLASS_H
#define SPECTHREADCLASS_H
#include <QtCore>
#include <QString>
#include <QThread>
#include <QVector>
#include <QStatusBar>
#include <QMutex>
#include <QFile>
#include <QTextStream>
#include <QList>
#include <plotspectraclass.h>

QT_BEGIN_NAMESPACE

namespace uiNameSpace {

extern bool isModeMean;


extern int noSpectrometers;

extern unsigned long totalRunTimeInmSec;
extern unsigned int totalRunTimeUnits;
extern unsigned long readingIntervalInmSec;
extern unsigned int readingIntervalUnits;

extern int const noPixels;
extern int xBeginPixel[8],xEndPixel[8];
extern float wavelengthToPixelFactor[8];
extern float minWavelength[8],maxWavelength[8];
extern float xMinGlobalWavelength,xMaxGlobalWavelength;
extern float beginScanFrequency,endScanFrequency;

extern QString spectrometerName[8];
extern unsigned long spectrometerExposureInmSec[8];

extern QString sampleType;
extern QString outputFilesDir;
extern QString oldFileNames[8];
extern bool openNewFiles[8];


extern unsigned short spectOutput[8][2048];
extern int scansAveraged;
extern int smoothingType,smoothingValue;

extern unsigned short spectOutput[8][2048];
extern unsigned short spectBackGroundSignal[8][2048];
extern bool isBackgroundAcquired;
extern bool isAcquiringLightNow;
extern bool acquireLightOnceNow;
extern bool isAcquiringDarkSignalNow;
extern bool isBackgroundNeedToBeSubtracted;
extern bool printDataHeadings[8];

extern QStatusBar *mainStatusBarObj;
extern plotSpectraClass *plotSpectraObj;

//data conversion
extern double fcoeffA[8][4];
extern double fcoeffB[8][4];

extern QList<QString> graphsTimeStamp[8];


} // namespace uiNameSpace

namespace filters {
extern bool isThresholdFilterEnabled;
extern int thresholdFilterPercentofPixels,thresholdFilterBaseLine;

extern bool isDistanceBasedFilterEnabled;

}

QT_END_NAMESPACE


class specThreadClass : public QThread
{    Q_OBJECT

    unsigned int spectrometerIndex,currentChannel;
    bool quitThread=false;
    QString timeStamp,dateStamp;
    QMutex generalLocl;
    QFile outputFileName;
    QTextStream writeToFile;

    double fCoefsA[4];
    double fCoefsB[4];




public:
    specThreadClass();
    ~specThreadClass();
    void run();

signals:
    void quitImediately();
    void plotNewData(int,int);
    void updateProgressBar();
    void resetPlotParameters(int);


public slots:
    void setSpectrometerIndex(unsigned int,unsigned int);
    void quitThreadNow();

};

#endif // SPECTHREADCLASS_H
