#ifndef PREVIEWSPECTRACLASS_H
#define PREVIEWSPECTRACLASS_H

#include <QWidget>
#include "qcustomplot.h"
#include <QString>

class previewSpectraClass : public QWidget
{

    Q_OBJECT

    QGroupBox *BSF,*DBF,*OPF,*SIDF;

    QSlider *BSFSlider,*DBFSlider,*OPFSlider,*SIDFSlider;
    QSlider *minDBFRangeSlider,*maxDBFRangeSlider;
    QSlider *minOPFRangeSlider,*maxOPFRangeSlider;
    QSlider *minSIDFRangeSlider,*maxSIDFRangeSlider;

    QLabel *BSFLabel,*DBFLabel,*OPFLabel,*SIDFLabel;
    QLabel *minDBFRangeLabel[2], *maxDBFRangeLabel[2],*minOPFRangeLabel[2], *maxOPFRangeLabel[2],*minSIDFRangeLabel[2], *maxSIDFRangeLabel[2];

    QCheckBox *BSFCheckBox,*DBFCheckBox,*OPFCheckBox,*SIDFCheckBox;

    int noReadingInList,noReadingInFilterList,minDBFRangeFilter,maxDBFRangeFilter,minOPFRangeFilter,maxOPFRangeFilter,minSIDFRangeFilter,maxSIDFRangeFilter;
    int BSFilterGraphNumber=1000000,DBFilterGraphNumber=1000000,OPFilterGraphNumber=1000000,SIDFilterGraphNumber=1000000;
    QCustomPlot *previewSpectraPlotObj;
    int spectraSlidersLastValue=0,filtersSliderSlidersLastValue=0;
    int DBFGraphIndex;
    int numGraphs=0,numGraphsLoaded=0;
    float EuclideanDistance;

    QList<double> xList;
    QList<double> yList[100000];

    QList<double> xFilter,yFilter;
    QList<float>  EuclideanDistanceList,SAMOPList,discrepanciesSID;
    QList<bool> isSurvivesDBFilterList,isSurvivesBSFilterList,isSurvivesOPFilterList,isSurvivesSIDFilterList;

    bool isFilterChoosen=false,isFilterLoaded=false;

    bool isShowAllGraphsButtonClicked=true;
    QSlider *spectraSlider,*filtersSlider;
    QPushButton *showAllGraphsButton;

    QLabel *refFilterStatusLabel,*DBFilterLabel,*currentGraphDBFilterLabel,*currentGraphDBFilterValueLabel,*percentGraphsDBSurvivors;
    QLabel *BSFilterLabel,*currentGraphBSFilterLabel,*currentGraphBSFilterValueLabel,*percentGraphsBSSurvivors;
    QLabel *OPFilterLabel,*currentGraphOPFilterLabel,*currentGraphOPFilterValueLabel,*percentGraphsOPSurvivors;
    QLabel *SIDFilterLabel,*currentGraphSIDFilterLabel,*currentGraphSIDFilterValueLabel,*percentGraphsSIDSurvivors;

    float DBFilterMaxValue=1.0,OPFilterMaxValue=1.0,SIDFilterMaxValue=1.0;
    QPushButton *exportSurvivedSpectraButton,*setRefFilterButton,*exportRefSpectraButton,*loadRefSpectraButton,*exportSettingsButton;

    QString exportFileName;
private:
    void makeWindowItems();
    void readDataInFile(QString);
    void baselineSurvivingFilter();
    void distanceBasedFilter();
    void orthoProjectionFilter();
    void spectralInfoDivFilter();
    void updateDBFilterDisplayInformation();
    void updateBSFilterDisplayInformation();
    void updateOPFilterDisplayInformation();

public:

    explicit previewSpectraClass(QWidget *parent = 0,QString spectraFileName="");
    ~previewSpectraClass();

signals:

public slots:

private slots:
    void filtersBrowser();
    void spectraBrowser();
    void showAllorSoleSpectra();
    void setSpectrumAsFilter();
    void applyBSFilterToSpectra();
    void applyDBFilterToSpectra();
    void applyOPFilterToSpectra();
    void applySIDFilterToSpectra();

    void clearChoosedFilterFunction();

    // void updateDBOPFilterSurvivors();
    void updateOPFilterSurvivors();
    void updateSIDFilterSurvivors();

    void viewBSFilterRangeOnGraph(bool);
    void viewDBFilterRangeOnGraph(bool);
    void viewOPFilterRangeOnGraph(bool);
    void viewSIDFilterRangeOnGraph(bool);

    void BSFUpdateFilterBaselineValue();
    void DBFUpdateFilterRange();
    void OPFUpdateFilterRange();
    void SIDFUpdateFilterRange();

    void updateMaxDBFilterSliderValue(double);
    void updateMaxOPFilterSliderValue(double);
    void updateMaxSIDFilterSliderValue(double);

    void updateSIDFilterDisplayInformation();

    void updateDBFilterThresholdAndInformation();

    void exportRefSpectrum();
    void loadRefSpectrum();
    void exportFilterSettings();
    void exportSurvivedSpectrum();
};

#endif // PREVIEWSPECTRACLASS_H
