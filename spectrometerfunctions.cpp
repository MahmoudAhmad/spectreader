#include "spectrometerfunctions.h"
#include "specthreadclass.h"
#include "mainwindow.h"
#include "bwtekusb.h"
#include <QDir>
#include <QApplication>
#include <QFile>
#include <QDebug>
#include <QSettings>
#include <QThread>
#include <QDebug>

namespace uiNameSpace {

bool initiateSpectrometers()
{
    bool isOk=true;
    bool isDeviceReady=false;

    QLibrary specLibrary("BWTEKUSB.DLL");
    if(!specLibrary.isLoaded())
        specLibrary.load();

    typedef int (__stdcall *InitDevicesFunc)();
    InitDevicesFunc InitDevicesObj = (InitDevicesFunc) specLibrary.resolve("InitDevices");
    isDeviceReady =InitDevicesObj();
    if (!isDeviceReady)
        isOk=false;

// Assign channels dynamically
    unsigned char nChannelStatus[32];
    int noSpects=-1;

    typedef int (__stdcall *bwtekSetupChannelFunc)(int,unsigned char *);
    bwtekSetupChannelFunc bwtekSetupChannelObj = (bwtekSetupChannelFunc) specLibrary.resolve("bwtekSetupChannel");
    noSpects =bwtekSetupChannelObj(-1, &nChannelStatus[0]);
    if (noSpects<1)
        isOk=false;


//export eeprom data
    QDir eepromFilesDir(QApplication::applicationDirPath()+"/eepromFiles/");
    if (!eepromFilesDir.exists())
        eepromFilesDir.mkdir(QApplication::applicationDirPath()+"/eepromFiles/");

    QDir outputFilesDir(QApplication::applicationDirPath()+"/outputFiles/");
    if (!outputFilesDir.exists())
        outputFilesDir.mkdir(QApplication::applicationDirPath()+"/outputFiles/");

    uiNameSpace::outputFilesDir=outputFilesDir.absolutePath();

// Number of spectrometers
    uiNameSpace::noSpectrometers=0;

    typedef int (__stdcall *GetDeviceCountFunc)();
    GetDeviceCountFunc GetDeviceCountObj = (GetDeviceCountFunc) specLibrary.resolve("GetDeviceCount");

    uiNameSpace::noSpectrometers =GetDeviceCountObj();
    if (uiNameSpace::noSpectrometers<1)
        return isOk=false;

// acquire eeprom files from spectrometers

    int isAccomplished=0;
    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        QByteArray eepromFileNameArray;
        eepromFileNameArray.append(eepromFilesDir.path()+"/parameters_ch"+QString::number(i)+".ini");

        typedef int (__stdcall *bwtekReadEEPROMUSBFunc)(char eepromFileName[],int);
        bwtekReadEEPROMUSBFunc bwtekReadEEPROMUSBObj = (bwtekReadEEPROMUSBFunc) specLibrary.resolve("bwtekReadEEPROMUSB");

        isAccomplished =bwtekReadEEPROMUSBObj(eepromFileNameArray.data(),i);
        if(isAccomplished<0)
            isOk=false;

        QSettings iniFileName(eepromFileNameArray.data(),QSettings::IniFormat);
        iniFileName.beginGroup("COLOR");
            uiNameSpace::minWavelength[i]= iniFileName.value("first_wavelength_of_color_calc").toFloat();
            uiNameSpace::maxWavelength[i] = iniFileName.value("end_wavelength_of_color_calc").toFloat();
        iniFileName.endGroup();

        iniFileName.beginGroup("COMMON");
        for (int j=0;j<4;j++)
        {
            uiNameSpace::fcoeffA[i][j]= iniFileName.value("coefs_a"+QString::number(j)).toDouble();
            uiNameSpace::fcoeffB[i][j]= iniFileName.value("coefs_b"+QString::number(j)).toDouble();
        }

            uiNameSpace::spectC_Code[i]= iniFileName.value("c_code").toString();
        iniFileName.endGroup();

    }

    uiNameSpace::xMinGlobalWavelength=0;   uiNameSpace::xMaxGlobalWavelength=10000;
    for(int i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        if (uiNameSpace::xMinGlobalWavelength >= uiNameSpace::minWavelength[i])
        {
            uiNameSpace::xMinGlobalWavelength=uiNameSpace::minWavelength[i];
        }

        if (uiNameSpace::xMaxGlobalWavelength <= uiNameSpace::maxWavelength[i])
        {
            uiNameSpace::xMaxGlobalWavelength=uiNameSpace::maxWavelength[i];
        }

    }

    if (!isOk and specLibrary.isLoaded())
    {
        specLibrary.unload();
    }

    return isOk;

} // initiateSpectrometers()

void shutdownSpectrometers()
{
    QLibrary specLibrary("BWTEKUSB.DLL");
    if(specLibrary.isLoaded())
    {
        typedef int (__stdcall *CloseDevicesFunc)();
        CloseDevicesFunc CloseDevicesObj = (CloseDevicesFunc) specLibrary.resolve("CloseDevices");
        CloseDevicesObj();
        specLibrary.unload();
    }
}

}// namespace uiNameSpace

