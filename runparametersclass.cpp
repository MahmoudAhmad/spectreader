#include "runparametersclass.h"
#include <QComboBox>
#include <QVBoxLayout>
#include <QFrame>
#include <QMessageBox>
#include <QGroupBox>
#include <QLabel>
#include <QString>
#include <QDebug>
#include <QtMath>
#include <QFile>
#include <QTextCodec>

runParametersClass::runParametersClass(QWidget *parent) : QWidget(parent)
{
    paletteWhite.setColor(QPalette::Base,Qt::white);
    paletteYellow.setColor(QPalette::Base,Qt::yellow);

    connect(this,SIGNAL(updateNewFrequencyRange()),uiNameSpace::plotSpectraObj,SLOT(setNewFrequencyRange()));
    connect(uiNameSpace::mainToolBarObj,SIGNAL(exportRunParameters()),this,SLOT(exportRunParameters()));
    connect(uiNameSpace::mainToolBarObj,SIGNAL(readRunParameters()),this,SLOT(readRunParameters()));

    QGroupBox *runParametersGroupBox=new QGroupBox("Run Parameters");
    runParametersGroupBox->setStyleSheet("QGroupBox::title {color : rgb(0,0,255);}");
    runParametersGroupBox->setFont(uiNameSpace::WinXFontM);

//total runtime
    QLabel *totalRunTimeLabel=new QLabel("Total RunTime:");
    totalRunTimeComboBox->addItem("msec");
    totalRunTimeComboBox->addItem("sec");
    totalRunTimeComboBox->addItem("min");
    totalRunTimeComboBox->addItem("hr");

    QHBoxLayout *totalRunTimeHLayout = new QHBoxLayout;
    totalRunTimeHLayout->addWidget(totalRunTimeLabel);
    totalRunTimeHLayout->addWidget(totalRunTimeLineEdit);
    totalRunTimeHLayout->addWidget(totalRunTimeComboBox);

    totalRunTimeLineEdit->setText("1");
    totalRunTimeLineEdit->setAlignment(Qt::AlignCenter);
    totalRunTimeComboBox->setCurrentText("min");
    connect(totalRunTimeLineEdit,SIGNAL(textChanged(QString)),this,SLOT(warnUserwithChangeTotalRunTime()));
    connect(totalRunTimeLineEdit,SIGNAL(editingFinished()),this,SLOT(setTotalRunTime()));
    connect(totalRunTimeComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(setTotalRunTime()));
    uiNameSpace::totalRunTimeInmSec=60000;

//reading interval
    QLabel *readingsIntervalLabel=new QLabel("Reading Interval:");
    readingIntervalComboBox->addItem("msec");
    readingIntervalComboBox->addItem("sec");
    readingIntervalComboBox->addItem("min");

    readingIntervalLineEdit->setText("500");
    readingIntervalLineEdit->setAlignment(Qt::AlignCenter);
    readingIntervalComboBox->setCurrentText("msec");
    connect(readingIntervalLineEdit,SIGNAL(textChanged(QString)),this,SLOT(warnUserChangedReadingInterval()));
    connect(readingIntervalLineEdit,SIGNAL(editingFinished()),this,SLOT(setReadingInterval()));
    connect(readingIntervalComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(setReadingInterval()));

    QHBoxLayout *readingsIntervalHLayout = new QHBoxLayout;
    readingsIntervalHLayout->addWidget(readingsIntervalLabel);
    readingsIntervalHLayout->addWidget(readingIntervalLineEdit);
    readingsIntervalHLayout->addWidget(readingIntervalComboBox);
    uiNameSpace::readingIntervalInmSec=500;

//scan range
    QLabel *scanRangeLabel=new QLabel("Scan Range (nm):");
    beginScanRangeLineEdit = new QLineEdit("300");
    beginScanRangeLineEdit->setAlignment(Qt::AlignCenter);
    QLabel *toLabel=new QLabel("to");
    endScanRangeLineEdit = new QLineEdit("900");
    endScanRangeLineEdit->setAlignment(Qt::AlignCenter);

    QHBoxLayout *scanRangeHLayout = new QHBoxLayout;
    scanRangeHLayout->addWidget(scanRangeLabel);
    scanRangeHLayout->addWidget(beginScanRangeLineEdit);
    scanRangeHLayout->addWidget(toLabel);
    scanRangeHLayout->addWidget(endScanRangeLineEdit);

    connect(beginScanRangeLineEdit,SIGNAL(textChanged(QString)),this,SLOT(warnUserBeginFrequencyChange()));
    connect(endScanRangeLineEdit,SIGNAL(textChanged(QString)),this,SLOT(warnUserEndFrequencyChange()));
    connect(beginScanRangeLineEdit,SIGNAL(editingFinished()),this,SLOT(setFrequencyRange()));
    connect(endScanRangeLineEdit,SIGNAL(editingFinished()),this,SLOT(setFrequencyRange()));
    connect(this,SIGNAL(updateNewFrequencyRange()),uiNameSpace::plotSpectraObj,SLOT(setNewFrequencyRange()));
    uiNameSpace::beginScanFrequency=300;
    uiNameSpace::endScanFrequency=900;

    for (unsigned short i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        uiNameSpace::xBeginPixel[i]=(int)(uiNameSpace::fcoeffB[i][0]+uiNameSpace::fcoeffB[i][1]*uiNameSpace::beginScanFrequency+uiNameSpace::fcoeffB[i][2]*qPow(uiNameSpace::beginScanFrequency,2.)+uiNameSpace::fcoeffB[i][3]*qPow(uiNameSpace::beginScanFrequency,3.));
        uiNameSpace::xEndPixel[i]=(int)(uiNameSpace::fcoeffB[i][0]+uiNameSpace::fcoeffB[i][1]*uiNameSpace::endScanFrequency+uiNameSpace::fcoeffB[i][2]*qPow(uiNameSpace::endScanFrequency,2.)+uiNameSpace::fcoeffB[i][3]*qPow(uiNameSpace::endScanFrequency,3.));

        if(uiNameSpace::xBeginPixel[i] < 0)
            uiNameSpace::xBeginPixel[i]=0;
        if(uiNameSpace::xEndPixel[i] > 2047)
            uiNameSpace::xEndPixel[i]=2047;
    }

    //line Separator
    QFrame* line = new QFrame;
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

// smoothing paramerers
    QLabel *scanModeLabel=new QLabel("Scan Mode:");
    scanModeComboBox=new QComboBox;
    scanModeComboBox->addItem("Mean");
    scanModeComboBox->addItem("Median");
    scanModeComboBox->setCurrentText("Mean");
    connect(scanModeComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(setScanMode()));
    uiNameSpace::isModeMean=true;

    QHBoxLayout *scanModeHLayout = new QHBoxLayout;
    scanModeHLayout->addWidget(scanModeLabel);
    scanModeHLayout->addWidget(scanModeComboBox);

// scans averaged
    QLabel *scansAveragedLabel=new QLabel("Scans Averaged per Reading:");
    scansAveragedLineEdit=new QLineEdit("1");
    scansAveragedLineEdit->setAlignment(Qt::AlignCenter);
    QHBoxLayout *scansAveragedHLayout = new QHBoxLayout;
    scansAveragedHLayout->addWidget(scansAveragedLabel);
    scansAveragedHLayout->addWidget(scansAveragedLineEdit);
    connect(scansAveragedLineEdit,SIGNAL(textChanged(QString)),this,SLOT(warnUsersScansAveragedPerReadingChanged()));
    connect(scansAveragedLineEdit,SIGNAL(editingFinished()),this,SLOT(setScansAveragedPerReading()));
    uiNameSpace::scansAveraged=1;

//smoothing type
    QLabel *smoothingTypeLabel=new QLabel("Smoothing Type:");
    smoothingTypeComboBox = new QComboBox;
    smoothingTypeComboBox->addItem("No smoothing");
    smoothingTypeComboBox->addItem("FFT smoothing");
    smoothingTypeComboBox->addItem("Savitzky-Golay");
    smoothingTypeComboBox->addItem("Boxcar smoothing");
    smoothingTypeComboBox->setCurrentText("No smoothing");
    connect(smoothingTypeComboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(setSmoothingType()));
    uiNameSpace::smoothingType=0;

    QHBoxLayout *smoothingTypeHLayout = new QHBoxLayout;
    smoothingTypeHLayout->addWidget(smoothingTypeLabel);
    smoothingTypeHLayout->addWidget(smoothingTypeComboBox);

// smoothing value
    QLabel *smoothingValueLabel=new QLabel("Smoothing Value:");
    smoothingValueLineEdit = new QLineEdit("0");
    smoothingValueLineEdit->setAlignment(Qt::AlignCenter);
    smoothingValueLineEdit->setEnabled(false);
    connect(smoothingValueLineEdit,SIGNAL(textChanged(QString)),this,SLOT(warnUserSmoothingValueChanged()));
    connect(smoothingValueLineEdit,SIGNAL(editingFinished()),this,SLOT(setSmoothingValue()));
    uiNameSpace::smoothingValue=0;

// layouts
    QHBoxLayout *smoothingValueHLayout = new QHBoxLayout;
    smoothingValueHLayout->addWidget(smoothingValueLabel);
    smoothingValueHLayout->addWidget(smoothingValueLineEdit);

    QVBoxLayout *runParametersVLayout=new QVBoxLayout;
    runParametersVLayout->addLayout(totalRunTimeHLayout);
    runParametersVLayout->addLayout(readingsIntervalHLayout);
    runParametersVLayout->addLayout(scanRangeHLayout);
    runParametersVLayout->addWidget(line);
    runParametersVLayout->addLayout(scanModeHLayout);
    runParametersVLayout->addLayout(scansAveragedHLayout);
    runParametersVLayout->addLayout(smoothingTypeHLayout);
    runParametersVLayout->addLayout(smoothingValueHLayout);

    QGridLayout *runParametersLayout = new QGridLayout;
    runParametersGroupBox->setLayout(runParametersVLayout);
    runParametersGroupBox->setFixedSize(300,240);
    runParametersLayout->addWidget(runParametersGroupBox);
    setLayout(runParametersLayout);
}

runParametersClass::~runParametersClass()
{

}

void runParametersClass::setTotalRunTime()
{
    if (this->totalRunTimeComboBox->currentText()=="msec")
    {
        uiNameSpace::totalRunTimeUnits=1;
    }
    else if (this->totalRunTimeComboBox->currentText()=="sec")
    {
        uiNameSpace::totalRunTimeUnits=1000;
    }
    else if (this->totalRunTimeComboBox->currentText()=="min")
    {
        uiNameSpace::totalRunTimeUnits=60000;
    }
    else if (this->totalRunTimeComboBox->currentText()=="hr")
    {
        uiNameSpace::totalRunTimeUnits=3600000;
    }

    if (totalRunTimeLineEdit->text().toDouble()*uiNameSpace::totalRunTimeUnits <= 0){
        QMessageBox::information(this,"Warning","Total runTime must be > 0 and < 1 Week");
        uiNameSpace::totalRunTimeInmSec = 1000;
        this->totalRunTimeLineEdit->setText("1");
        this->totalRunTimeComboBox->setCurrentText("sec");
    }
    else if (totalRunTimeLineEdit->text().toDouble()*uiNameSpace::totalRunTimeUnits > 604800000)
    {
        QMessageBox::information(this,"Input Error","Total runTime must be > 0 and < 1 Week");
        uiNameSpace::totalRunTimeInmSec = 86400000;
        this->totalRunTimeLineEdit->setText("24");
        this->totalRunTimeComboBox->setCurrentText("hr");
    }
    else
    {
        uiNameSpace::totalRunTimeInmSec=this->totalRunTimeLineEdit->text().toDouble()*uiNameSpace::totalRunTimeUnits;
    }

    totalRunTimeLineEdit->setPalette(paletteWhite);
}

void runParametersClass::setReadingInterval()
{
    if (this->readingIntervalComboBox->currentText()=="msec")
    {
        uiNameSpace::readingIntervalUnits=1;
    }
    else if (this->readingIntervalComboBox->currentText()=="sec")
    {
        uiNameSpace::readingIntervalUnits=1000;
    }
    else if (this->readingIntervalComboBox->currentText()=="min")
    {
        uiNameSpace::readingIntervalUnits=60000;
    }


    if (readingIntervalLineEdit->text().toDouble()*uiNameSpace::readingIntervalUnits <= 0){
        QMessageBox::information(this,"Input Error","reading Interval must be > 0 and < 30 min");
        uiNameSpace::readingIntervalInmSec = 100;
        this->readingIntervalLineEdit->setText("100");
        this->readingIntervalComboBox->setCurrentText("msec");
    }
    else if (readingIntervalLineEdit->text().toDouble()*uiNameSpace::readingIntervalUnits > 1800000)
    {
        QMessageBox::information(this,"Input Error","Total runTime must be > 0 and < 30 min");
        uiNameSpace::readingIntervalInmSec = 1800000;
        this->readingIntervalLineEdit->setText("30");
        this->readingIntervalComboBox->setCurrentText("min");
    }
    else
    {
        uiNameSpace::readingIntervalInmSec=this->readingIntervalLineEdit->text().toLong()*uiNameSpace::readingIntervalUnits;
    }

    readingIntervalLineEdit->setPalette(paletteWhite);
}

void runParametersClass::setFrequencyRange()
{
    if (beginScanRangeLineEdit->text().toFloat()< uiNameSpace::xMinGlobalWavelength)
    {
        QMessageBox::information(this,"Warning","minimun scan frequency must be >" +QString::number(uiNameSpace::xMinGlobalWavelength));
        //this->beginScanRangeLineEdit->setText(QString::number(300));
        return;
    }
    else if (beginScanRangeLineEdit->text().toFloat() > endScanRangeLineEdit->text().toFloat())
    {
        QMessageBox::information(this,"Warning","minimun scan frequency can not exceed maximum scan frequency");
       // this->beginScanRangeLineEdit->setText("300");
        return;
    }
    else
    {
        uiNameSpace::beginScanFrequency=beginScanRangeLineEdit->text().toFloat();
        beginScanRangeLineEdit->setPalette(paletteWhite);
    }

    if (endScanRangeLineEdit->text().toFloat() > uiNameSpace::xMaxGlobalWavelength)
    {
        QMessageBox::information(this,"Warning","maximum scan frequency must be <" +QString::number(uiNameSpace::xMaxGlobalWavelength));
        //this->endScanRangeLineEdit->setText(QString::number(900));
        return;
    }
    else if (endScanRangeLineEdit->text().toFloat() < beginScanRangeLineEdit->text().toFloat())
    {
        QMessageBox::information(this,"Warning","maximum scan frequency may not be less than minimum scan frequency");
        //this->endScanRangeLineEdit->setText(QString::number(900));
        return;
    }
    else
    {
        uiNameSpace::endScanFrequency=endScanRangeLineEdit->text().toFloat();
        endScanRangeLineEdit->setPalette(paletteWhite);
    }

//update minPixel and maxPixel per sectrometer

    for (unsigned short i=0;i<uiNameSpace::noSpectrometers;i++)
    {
        uiNameSpace::xBeginPixel[i]=(int)(uiNameSpace::fcoeffB[i][0]+uiNameSpace::fcoeffB[i][1]*uiNameSpace::beginScanFrequency+uiNameSpace::fcoeffB[i][2]*qPow(uiNameSpace::beginScanFrequency,2.)+uiNameSpace::fcoeffB[i][3]*qPow(uiNameSpace::beginScanFrequency,3.));
        uiNameSpace::xEndPixel[i]=(int)(uiNameSpace::fcoeffB[i][0]+uiNameSpace::fcoeffB[i][1]*uiNameSpace::endScanFrequency+uiNameSpace::fcoeffB[i][2]*qPow(uiNameSpace::endScanFrequency,2.)+uiNameSpace::fcoeffB[i][3]*qPow(uiNameSpace::endScanFrequency,3.));

        if(uiNameSpace::xBeginPixel[i] < 0)
            uiNameSpace::xBeginPixel[i]=0;
        if(uiNameSpace::xEndPixel[i] > 2047)
            uiNameSpace::xEndPixel[i]=2047;
    }
        emit updateNewFrequencyRange();
}

void runParametersClass::setScanMode()
{
    if (scanModeComboBox->currentText()=="Mean")
    {
        uiNameSpace::isModeMean=true;
    }
    else if (scanModeComboBox->currentText()=="Median")
    {
        uiNameSpace::isModeMean=false;
    }
}

void runParametersClass::setScansAveragedPerReading()
{
    if (scansAveragedLineEdit->text().toInt() < 1)
    {
        QMessageBox::information(this,"Warning","scans averaged must be >= 1");
        uiNameSpace::scansAveraged=1;
        this->scansAveragedLineEdit->setText("1");
    }
    else if (scansAveragedLineEdit->text().toInt() > 99)
    {
        QMessageBox::information(this,"Warning","scans averaged must be < 100");
        uiNameSpace::scansAveraged=99;
        this->scansAveragedLineEdit->setText("99");
    }
    else
    {
        uiNameSpace::scansAveraged=scansAveragedLineEdit->text().toInt();
        scansAveragedLineEdit->setPalette(paletteWhite);

    }
}

void runParametersClass::setSmoothingType()
{
    if (smoothingTypeComboBox->currentText()=="Boxcar smoothing")
    {
        uiNameSpace::smoothingType=3;
        this->smoothingValueLineEdit->setText("1");
        uiNameSpace::smoothingValue=1;
        smoothingValueLineEdit->setEnabled(true);
        smoothingValueLineEdit->setPalette(paletteWhite);
    }
    else if (smoothingTypeComboBox->currentText()=="Savitzky-Golay")
    {
        uiNameSpace::smoothingType=2;
        this->smoothingValueLineEdit->setText("2");
        uiNameSpace::smoothingValue=3;
        smoothingValueLineEdit->setEnabled(true);
        smoothingValueLineEdit->setPalette(paletteWhite);
    }
    else if (smoothingTypeComboBox->currentText()=="FFT smoothing")
    {
        uiNameSpace::smoothingType=1;
        this->smoothingValueLineEdit->setText("0");
        uiNameSpace::smoothingValue=0;
        smoothingValueLineEdit->setEnabled(true);
    smoothingValueLineEdit->setPalette(paletteWhite);
    }
    else
    {
        uiNameSpace::smoothingType=0;
        this->smoothingValueLineEdit->setText("0");
        uiNameSpace::smoothingValue=0;
        smoothingValueLineEdit->setEnabled(false);
        smoothingValueLineEdit->setPalette(paletteWhite);

    }
}

void runParametersClass::setSmoothingValue()
{
    if (smoothingTypeComboBox->currentText()=="Boxcar smoothing")
    {
        if ((smoothingValueLineEdit->text().toInt() < 1) or (smoothingValueLineEdit->text().toInt() > 100))
        {
            this->smoothingValueLineEdit->setText("1");
            uiNameSpace::smoothingValue=1;
            QMessageBox::information(this,"Warning","Boxcar smoothing value (1~100)");
        }
        else
        {
            uiNameSpace::smoothingValue=smoothingValueLineEdit->text().toInt();
        }
    }
    else if (smoothingTypeComboBox->currentText()=="Savitzky-Golay")
    {
        if ((smoothingValueLineEdit->text().toInt() < 2) or (smoothingValueLineEdit->text().toInt() > 5))
        {
            this->smoothingValueLineEdit->setText("2");
            uiNameSpace::smoothingValue=2;
            QMessageBox::information(this,"Warning","Savitzky-Golay value (2~5)");
        }
        else
        {
            uiNameSpace::smoothingValue=smoothingValueLineEdit->text().toInt();
        }
    }
    else if (smoothingTypeComboBox->currentText()=="FFT smoothing")
    {
        if ((smoothingValueLineEdit->text().toInt() < 0) or (smoothingValueLineEdit->text().toInt() > 100))
        {
            this->smoothingValueLineEdit->setText("0");
            uiNameSpace::smoothingValue=0;
            QMessageBox::information(this,"Warning","FFT smoothing value (0~100)");
        }
        else
        {
            uiNameSpace::smoothingValue=0;
        }
    }

    smoothingValueLineEdit->setPalette(paletteWhite);
}

void runParametersClass::warnUserwithChangeTotalRunTime()
{
    totalRunTimeLineEdit->setPalette(paletteYellow);
}

void runParametersClass::warnUserChangedReadingInterval()
{
    readingIntervalLineEdit->setPalette(paletteYellow);
}

void runParametersClass::warnUserBeginFrequencyChange()
{
    beginScanRangeLineEdit->setPalette(paletteYellow);
}

void runParametersClass::warnUserEndFrequencyChange()
{
    endScanRangeLineEdit->setPalette(paletteYellow);
}

void runParametersClass::warnUsersScansAveragedPerReadingChanged()
{
    scansAveragedLineEdit->setPalette(paletteYellow);
}

void runParametersClass::warnUserSmoothingValueChanged()
{
    smoothingValueLineEdit->setPalette(paletteYellow);
}

void runParametersClass::exportRunParameters()
{
    uiNameSpace::settings->beginGroup(QString("Run Parameters" ));
    uiNameSpace::settings->setValue("Total RunTime",totalRunTimeLineEdit->text());
    uiNameSpace::settings->setValue("Total RunTime Units",totalRunTimeComboBox->currentText());

    uiNameSpace::settings->setValue("Reading Interval",readingIntervalLineEdit->text());
    uiNameSpace::settings->setValue("Reading Interval Units",readingIntervalComboBox->currentText());

    uiNameSpace::settings->setValue("Begin Scan Range",beginScanRangeLineEdit->text());
    uiNameSpace::settings->setValue("END Scan Range",endScanRangeLineEdit->text());

    uiNameSpace::settings->setValue("Scan Mode",scanModeComboBox->currentText());
    uiNameSpace::settings->setValue("Scans Averaged",scansAveragedLineEdit->text());

    uiNameSpace::settings->setValue("Smoothing Type",smoothingTypeComboBox->currentText());
    uiNameSpace::settings->setValue("smoothing Value",smoothingValueLineEdit->text());

    uiNameSpace::settings->endGroup();
    uiNameSpace::settings->sync();


}

void runParametersClass::readRunParameters()
{

    uiNameSpace::settings->beginGroup("Run Parameters");

    totalRunTimeLineEdit->setText(uiNameSpace::settings->value("Total RunTime",totalRunTimeLineEdit->text()).toString());
    totalRunTimeComboBox->setCurrentText(uiNameSpace::settings->value("Total RunTime Units",totalRunTimeComboBox->currentText()).toString());

    readingIntervalLineEdit->setText(uiNameSpace::settings->value("Reading Interval",readingIntervalLineEdit->text()).toString());
    readingIntervalComboBox->setCurrentText(uiNameSpace::settings->value("Reading Interval Units",readingIntervalComboBox->currentText()).toString());

    beginScanRangeLineEdit->setText(uiNameSpace::settings->value("Begin Scan Range",beginScanRangeLineEdit->text()).toString());
    endScanRangeLineEdit->setText(uiNameSpace::settings->value("END Scan Range",endScanRangeLineEdit->text()).toString());

    scanModeComboBox->setCurrentText(uiNameSpace::settings->value("Scan Mode",scanModeComboBox->currentText()).toString());
    scansAveragedLineEdit->setText(uiNameSpace::settings->value("Scans Averaged",scansAveragedLineEdit->text()).toString());

    smoothingTypeComboBox->setCurrentText(uiNameSpace::settings->value("Smoothing Type",smoothingTypeComboBox->currentText()).toString());
    smoothingValueLineEdit->setText(uiNameSpace::settings->value("smoothing Value",smoothingValueLineEdit->text()).toString());

    uiNameSpace::settings->endGroup();
}

