#include "previewspectraclass.h"
#include "qcustomplot.h"
#include <QGridLayout>

previewSpectraClass::previewSpectraClass(QWidget *parent,QString spectraFileName) : QWidget(parent)
{
    // window settings
    setWindowTitle("Preview Spectra");
    setFixedSize(1080,552);

    previewSpectraPlotObj=new QCustomPlot;

    readDataInFile(spectraFileName);

    baselineSurvivingFilter();
    distanceBasedFilter();
    orthoProjectionFilter();
    spectralInfoDivFilter();
    makeWindowItems();
}
previewSpectraClass::~previewSpectraClass()
{

}
void previewSpectraClass::readDataInFile(QString spectraFileName)
{
    exportFileName=spectraFileName;
    QFile spectraFile(spectraFileName);
    if (!spectraFile.open(QIODevice::ReadWrite | QIODevice::Text))
            return;

    QTextStream inData(&spectraFile);
    QRegExp dataPattern("\\d\\d\\d\\d-\\d\\d-\\d\\d");
    QPen plotsPen(QColor(255,0,0,255));
    plotsPen.setStyle(Qt::SolidLine);
    plotsPen.setWidth(1);

    float freqRangeStart,freqRangeEnd;

    //count number of data
    while(!inData.atEnd())
    {
        QString currentLine = inData.readLine();
        if (dataPattern.exactMatch(currentLine.section("\t",0,0)))
        {
            numGraphs++;
        }
    }


    //read data
    inData.seek(0);
    numGraphsLoaded=0;
    QProgressDialog waitMSDialog("Loading Graphs ...", "Abort", 0, numGraphs, this);
    waitMSDialog.setWindowModality(Qt::WindowModal);
    waitMSDialog.setWindowTitle("Status");
    while(!inData.atEnd())
    {
        QString currentLine = inData.readLine();

        if(currentLine.startsWith("#Date"))
        {
            freqRangeStart=currentLine.section("\t",4,4).toFloat();
            noReadingInList=currentLine.count("\t")-4;// it should be 3 but there is an extra tab at the end

            freqRangeEnd=currentLine.section("\t",noReadingInList+4,noReadingInList+4).toFloat();
            for (int i=0; i<noReadingInList; i++)
            {
                xList << currentLine.section("\t",i+4,i+4).toFloat();
            }
        }
        else if (dataPattern.exactMatch(currentLine.section("\t",0,0)))
        {
            waitMSDialog.setValue(numGraphsLoaded);
            if(waitMSDialog.wasCanceled())
                break;
            for (int i=0; i<noReadingInList; i++)
            {
                    yList[numGraphsLoaded].append(currentLine.section("\t",i+4,i+4).toFloat()/65535.0);
            }
            numGraphsLoaded++;
        }
    }

    waitMSDialog.setValue(numGraphs);
    waitMSDialog.close();

    // plots data
    for (int i=0;i<numGraphsLoaded;i++)
    {
        QVector<double> xData=QVector<double>::fromList(xList);
        QVector<double> yData=QVector<double>::fromList(yList[i]);
        previewSpectraPlotObj->addGraph();
        previewSpectraPlotObj->graph()->setData(xData,yData);
        previewSpectraPlotObj->graph()->setPen(plotsPen);
    }

    previewSpectraPlotObj->legend->setFont(QFont("Helvetica",9));
    previewSpectraPlotObj->xAxis->setLabel("Wavelength (nm)");
    previewSpectraPlotObj->yAxis->setLabel("Absolute Intensity (Arb. Units)");
    previewSpectraPlotObj->xAxis->setRange(freqRangeStart,freqRangeEnd);
    previewSpectraPlotObj->yAxis->setRange(0,1.000001);
    previewSpectraPlotObj->yAxis->setAutoTickCount(5);
    previewSpectraPlotObj->xAxis->setAutoTickCount(9);
    previewSpectraPlotObj->rescaleAxes(true);
    previewSpectraPlotObj->axisRect()->setupFullAxesBox();
    previewSpectraPlotObj->setLocale(QLocale(QLocale::English, QLocale::UnitedKingdom));
    previewSpectraPlotObj->setInteraction(QCP::iRangeDrag,true);
    previewSpectraPlotObj->setInteraction(QCP::iRangeZoom,true);
    previewSpectraPlotObj->setInteraction(QCP::iSelectAxes,true);
    previewSpectraPlotObj->replot();

    // set filters ideces
    previewSpectraPlotObj->addGraph();
    previewSpectraPlotObj->addGraph();
    previewSpectraPlotObj->addGraph();
    previewSpectraPlotObj->addGraph();
    previewSpectraPlotObj->addGraph();
    BSFilterGraphNumber=numGraphsLoaded+1;
    DBFilterGraphNumber=numGraphsLoaded+2;
    OPFilterGraphNumber=numGraphsLoaded+3;
    SIDFilterGraphNumber=numGraphsLoaded+4;
    // set filters ideces

    //set initail legend
    previewSpectraPlotObj->legend->setVisible(true);
    previewSpectraPlotObj->legend->clearItems();
    previewSpectraPlotObj->graph(0)->setName("Data Spectrum");
    previewSpectraPlotObj->graph(0)->addToLegend();
    previewSpectraPlotObj->graph(0)->setName("Ref. Spectrum");
    //~set initial legend

}
void previewSpectraClass::makeWindowItems()
{
    QGridLayout *previewPlotLayout=new QGridLayout;

    // spectra slider
    spectraSlider=new QSlider(Qt::Horizontal);
    spectraSlider->setValue(0);
    spectraSlider->setDisabled(true);
    spectraSlider->setMaximum(numGraphsLoaded-1);
    connect(spectraSlider,SIGNAL(valueChanged(int)),this,SLOT(spectraBrowser()));
    // ~spectra slider

    // filter slider
    filtersSlider=new QSlider(Qt::Horizontal);
    filtersSlider->setValue(0);
    filtersSlider->setMaximum(numGraphsLoaded-1);
    connect(filtersSlider,SIGNAL(valueChanged(int)),this,SLOT(filtersBrowser()));
    // ~filter slider

    // Display information layout
    QGridLayout *informationDisplay=new QGridLayout;


    // Baseline surviving filter
    BSFilterLabel=                 new QLabel("Baseline Surviving Filter: Not Active");
    currentGraphBSFilterValueLabel=new QLabel("Distance from Reference: NA");
    currentGraphBSFilterLabel=     new QLabel("Active Spectrum Survives: NA");
    percentGraphsBSSurvivors=      new QLabel("Total Surviving Spectra: NA");
    informationDisplay->addWidget(BSFilterLabel,0,0);
    informationDisplay->addWidget(currentGraphBSFilterValueLabel,1,0);
    informationDisplay->addWidget(currentGraphBSFilterLabel,2,0);
    informationDisplay->addWidget(percentGraphsBSSurvivors,3,0);
    // ~Baseline surviving filter

    // DB filter information display
    DBFilterLabel=                 new QLabel("Distance-Based Filter: Not Active");
    currentGraphDBFilterValueLabel=new QLabel("Euclidean Distance from Ref: NA");
    currentGraphDBFilterLabel=     new QLabel("Active Spectrum Survives: NA");
    percentGraphsDBSurvivors=      new QLabel("Total Surviving Spectra: NA");
    informationDisplay->addWidget(DBFilterLabel,0,1);
    informationDisplay->addWidget(currentGraphDBFilterValueLabel,1,1);
    informationDisplay->addWidget(currentGraphDBFilterLabel,2,1);
    informationDisplay->addWidget(percentGraphsDBSurvivors,3,1);
    // ~DB filter information display

    // OP filter information display
    OPFilterLabel=                 new QLabel("Ortho.-Proj. Based Filter: Not Active");
    currentGraphOPFilterValueLabel=new QLabel("Divergence from Reference: NA");
    currentGraphOPFilterLabel=     new QLabel("Active Spectrum Survives: NA");
    percentGraphsOPSurvivors=      new QLabel("Total Surviving Spectra: NA");
    informationDisplay->addWidget(OPFilterLabel,0,2);
    informationDisplay->addWidget(currentGraphOPFilterValueLabel,1,2);
    informationDisplay->addWidget(currentGraphOPFilterLabel,2,2);
    informationDisplay->addWidget(percentGraphsOPSurvivors,3,2);
    // ~OP filter information display

    // OP filter information display
    SIDFilterLabel=                 new QLabel("Info.-Div. Based Filter: Not Active");
    currentGraphSIDFilterValueLabel=new QLabel("Discrepancy wrt Reference: NA");
    currentGraphSIDFilterLabel=     new QLabel("Active Spectrum Survives: NA");
    percentGraphsSIDSurvivors=      new QLabel("Total Surviving Spectra: NA");
    informationDisplay->addWidget(SIDFilterLabel,0,3);
    informationDisplay->addWidget(currentGraphSIDFilterValueLabel,1,3);
    informationDisplay->addWidget(currentGraphSIDFilterLabel,2,3);
    informationDisplay->addWidget(percentGraphsSIDSurvivors,3,3);
    // ~OP filter information display

    //export filter to file
    exportRefSpectraButton=new QPushButton("Export Ref");
    connect(exportRefSpectraButton,SIGNAL(clicked()),this,SLOT(exportRefSpectrum()));
    //~export filter to file

    //load filter from file
    loadRefSpectraButton=new QPushButton("Load Ref");
    connect(loadRefSpectraButton,SIGNAL(clicked()),this,SLOT(loadRefSpectrum()));
    loadRefSpectraButton->setIcon(QIcon(":/icons/resources/notDone.png"));
    //~load filter from file

    //export survivors to  file
    exportSurvivedSpectraButton=new QPushButton("Export Spectra");
    connect(exportSurvivedSpectraButton,SIGNAL(released()),this,SLOT(exportSurvivedSpectrum()));
    //~export survivors to  file

    //export settings to  file
    exportSettingsButton=new QPushButton("Export Settings");
    //connect(exportSurvivedSpectraButton,SIGNAL(released()),this,SLOT(exportSettings()));
    //~export survivors to  file

    showAllGraphsButton=new QPushButton("Show Sole Spectra");
    connect(showAllGraphsButton,SIGNAL(clicked()),this,SLOT(showAllorSoleSpectra()));

    // Button to choose filter
    setRefFilterButton=new QPushButton("Set Ref. Filter");
    setRefFilterButton->setIcon(QIcon(":/icons/resources/notDone.png"));
    connect(setRefFilterButton,SIGNAL(clicked()),this,SLOT(setSpectrumAsFilter()));
    // ~button to choose filter
    QHBoxLayout *refSpectraButtonLayout=new QHBoxLayout;
    refSpectraButtonLayout->addWidget(exportRefSpectraButton);
    refSpectraButtonLayout->addWidget(loadRefSpectraButton);

    QHBoxLayout *survivedSpectraAndSettingsLayout=new QHBoxLayout;
    survivedSpectraAndSettingsLayout->addWidget(exportSettingsButton);
    survivedSpectraAndSettingsLayout->addWidget(exportSurvivedSpectraButton);


    previewPlotLayout->addLayout(informationDisplay,0,0,2,8);
    previewPlotLayout->addWidget(previewSpectraPlotObj,2,0,6,8);
    previewPlotLayout->addWidget(spectraSlider,8,0,1,7);
    previewPlotLayout->addWidget(filtersSlider,9,0,1,7);
    previewPlotLayout->addWidget(BSF ,0,8,2,1);
    previewPlotLayout->addWidget(DBF ,2,8,2,1);
    previewPlotLayout->addWidget(OPF ,4,8,2,1);
    previewPlotLayout->addWidget(SIDF,6,8,2,1);
    previewPlotLayout->addWidget(showAllGraphsButton,8,7,1,1);
    previewPlotLayout->addWidget(setRefFilterButton,9,7,1,1);
    previewPlotLayout->addLayout(refSpectraButtonLayout,8,8,1,1);
    previewPlotLayout->addLayout(survivedSpectraAndSettingsLayout,9,8,1,1);

    setLayout(previewPlotLayout);
}
void previewSpectraClass::baselineSurvivingFilter()
{
    BSF=new QGroupBox;
    BSF->setTitle("Baseline Surviving Filter");
    BSF->setCheckable(true);

    BSFSlider=new QSlider(Qt::Horizontal);
    BSFSlider->setRange(0,1000);
    BSFSlider->setValue(0);
    connect(BSFSlider,SIGNAL(valueChanged(int)),this,SLOT(BSFUpdateFilterBaselineValue()));
    connect(BSFSlider,SIGNAL(valueChanged(int)),this,SLOT(applyBSFilterToSpectra()));

    BSFLabel=new QLabel("0");
    BSFCheckBox=new QCheckBox("View baseline (Grey)");
    connect(BSFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewBSFilterRangeOnGraph(bool)));

    QGridLayout *BSFGridLayout=new QGridLayout(BSF);
    BSFGridLayout->addWidget(BSFCheckBox,0,0,1,3);
    BSFGridLayout->addWidget(BSFSlider,1,0,1,3);
    BSFGridLayout->addWidget(BSFLabel,1,3,1,1);
}
void previewSpectraClass::distanceBasedFilter()
{
    DBF=new QGroupBox;
    DBF->setTitle("Distance-Based Filter");
    DBF->setCheckable(true);

    DBFSlider=new QSlider(Qt::Horizontal);
    DBFSlider->setRange(0,1000);
    DBFSlider->setValue(0);
    connect(DBFSlider,SIGNAL(valueChanged(int)),this,SLOT(updateDBFilterThresholdAndInformation()));

    QDoubleSpinBox *DBFilterMaxValueSpinBox=new QDoubleSpinBox;
    DBFilterMaxValueSpinBox->setSingleStep(0.01);
    DBFilterMaxValueSpinBox->setValue(1.00);
    DBFilterMaxValueSpinBox->setRange(0.0,100.0);
    connect(DBFilterMaxValueSpinBox,SIGNAL(valueChanged(double)),this,SLOT(updateMaxDBFilterSliderValue(double)));

    minDBFRangeSlider=new QSlider(Qt::Horizontal);
    maxDBFRangeSlider=new QSlider(Qt::Horizontal);

    minDBFRangeSlider->setRange(0,noReadingInList-1);
    maxDBFRangeSlider->setRange(0,noReadingInList-1);

    minDBFRangeSlider->setValue(0);
    maxDBFRangeSlider->setValue(noReadingInList-1);
    minDBFRangeFilter=0;
    maxDBFRangeFilter=noReadingInList-1;

    connect(minDBFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(DBFUpdateFilterRange()));
    connect(maxDBFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(DBFUpdateFilterRange()));
    connect(minDBFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyDBFilterToSpectra()));
    connect(maxDBFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyDBFilterToSpectra()));

    DBFLabel=new QLabel("0");

    DBFCheckBox=new QCheckBox("View Range (Yellow)");
    connect(DBFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewDBFilterRangeOnGraph(bool)));
    minDBFRangeLabel[0]=new QLabel("from:");
    minDBFRangeLabel[1]=new QLabel("min");
    maxDBFRangeLabel[0]=new QLabel("to:");
    maxDBFRangeLabel[1]=new QLabel("max");

    QGridLayout *BDFGridLayout=new QGridLayout(DBF);
    BDFGridLayout->addWidget(DBFSlider,0,0,1,2);
    BDFGridLayout->addWidget(DBFilterMaxValueSpinBox,0,2,1,1);
    BDFGridLayout->addWidget(DBFLabel,0,3,1,1);
    BDFGridLayout->addWidget(DBFCheckBox,1,0,1,4);
    BDFGridLayout->addWidget(minDBFRangeLabel[0],2,0,1,1);
    BDFGridLayout->addWidget(minDBFRangeLabel[1],2,1,1,1);
    BDFGridLayout->addWidget(minDBFRangeSlider,2,2,1,2);
    BDFGridLayout->addWidget(maxDBFRangeLabel[0],3,0,1,1);
    BDFGridLayout->addWidget(maxDBFRangeLabel[1],3,1,1,1);
    BDFGridLayout->addWidget(maxDBFRangeSlider,3,2,1,2);
}
void previewSpectraClass::orthoProjectionFilter()
{
    OPF=new QGroupBox;
    OPF->setTitle("Ortho-Projection Filter");
    OPF->setCheckable(true);

    OPFSlider=new QSlider(Qt::Horizontal);
    OPFSlider->setRange(0,1000);
    OPFSlider->setValue(0);
    connect(OPFSlider,SIGNAL(valueChanged(int)),this,SLOT(updateOPFilterSurvivors()));

    QDoubleSpinBox *OPFilterMaxValueSpinBox=new QDoubleSpinBox;
    OPFilterMaxValueSpinBox->setSingleStep(0.01);
    OPFilterMaxValueSpinBox->setValue(1.00);
    OPFilterMaxValueSpinBox->setRange(0.0,100.0);
    connect(OPFilterMaxValueSpinBox,SIGNAL(valueChanged(double)),this,SLOT(updateMaxOPFilterSliderValue(double)));

    minOPFRangeSlider=new QSlider(Qt::Horizontal);
    maxOPFRangeSlider=new QSlider(Qt::Horizontal);

    minOPFRangeSlider->setRange(0,noReadingInList-1);
    maxOPFRangeSlider->setRange(0,noReadingInList-1);

    minOPFRangeSlider->setValue(0);
    maxOPFRangeSlider->setValue(noReadingInList-1);
    minOPFRangeFilter=0;
    maxOPFRangeFilter=noReadingInList-1;

    connect(minOPFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(OPFUpdateFilterRange()));
    connect(maxOPFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(OPFUpdateFilterRange()));
    connect(minOPFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyOPFilterToSpectra()));
    connect(maxOPFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applyOPFilterToSpectra()));

    OPFLabel=new QLabel("0");

    OPFCheckBox=new QCheckBox("View Range (Green)");
    connect(OPFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewOPFilterRangeOnGraph(bool)));
    minOPFRangeLabel[0]=new QLabel("from:");
    minOPFRangeLabel[1]=new QLabel("min");
    maxOPFRangeLabel[0]=new QLabel("to:");
    maxOPFRangeLabel[1]=new QLabel("max");

    QGridLayout *OPFGridLayout=new QGridLayout(OPF);
    OPFGridLayout->addWidget(OPFSlider,0,0,1,2);
    OPFGridLayout->addWidget(OPFilterMaxValueSpinBox,0,2,1,1);
    OPFGridLayout->addWidget(OPFLabel,0,3,1,1);
    OPFGridLayout->addWidget(OPFCheckBox,1,0,1,4);
    OPFGridLayout->addWidget(minOPFRangeLabel[0],2,0,1,1);
    OPFGridLayout->addWidget(minOPFRangeLabel[1],2,1,1,1);
    OPFGridLayout->addWidget(minOPFRangeSlider,2,2,1,2);
    OPFGridLayout->addWidget(maxOPFRangeLabel[0],3,0,1,1);
    OPFGridLayout->addWidget(maxOPFRangeLabel[1],3,1,1,1);
    OPFGridLayout->addWidget(maxOPFRangeSlider,3,2,1,2);
}
void previewSpectraClass::spectralInfoDivFilter()
{
    SIDF=new QGroupBox;
    SIDF->setTitle("Spectral Info. Div Filter");
    SIDF->setCheckable(true);

    SIDFSlider=new QSlider(Qt::Horizontal);
    SIDFSlider->setRange(0,1000);
    SIDFSlider->setValue(0);
    connect(SIDFSlider,SIGNAL(valueChanged(int)),this,SLOT(updateSIDFilterSurvivors()));

    QDoubleSpinBox *SIDFilterMaxValueSpinBox=new QDoubleSpinBox;
    SIDFilterMaxValueSpinBox->setSingleStep(0.01);
    SIDFilterMaxValueSpinBox->setValue(1.00);
    SIDFilterMaxValueSpinBox->setRange(0.0,100.0);
    connect(SIDFilterMaxValueSpinBox,SIGNAL(valueChanged(double)),this,SLOT(updateMaxSIDFilterSliderValue(double)));

    minSIDFRangeSlider=new QSlider(Qt::Horizontal);
    maxSIDFRangeSlider=new QSlider(Qt::Horizontal);

    minSIDFRangeSlider->setRange(0,noReadingInList-1);
    maxSIDFRangeSlider->setRange(0,noReadingInList-1);

    minSIDFRangeSlider->setValue(0);
    maxSIDFRangeSlider->setValue(noReadingInList-1);
    minSIDFRangeFilter=0;
    maxSIDFRangeFilter=noReadingInList-1;

    connect(minSIDFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(SIDFUpdateFilterRange()));
    connect(maxSIDFRangeSlider,SIGNAL(valueChanged(int)),this,SLOT(SIDFUpdateFilterRange()));
    connect(minSIDFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applySIDFilterToSpectra()));
    connect(maxSIDFRangeSlider,SIGNAL(sliderReleased()),this,SLOT(applySIDFilterToSpectra()));

    SIDFLabel=new QLabel("0");

    SIDFCheckBox=new QCheckBox("View Range (Blue)");
    connect(SIDFCheckBox,SIGNAL(clicked(bool)),this,SLOT(viewSIDFilterRangeOnGraph(bool)));
    minSIDFRangeLabel[0]=new QLabel("from:");
    minSIDFRangeLabel[1]=new QLabel("min");
    maxSIDFRangeLabel[0]=new QLabel("to:");
    maxSIDFRangeLabel[1]=new QLabel("max");

    QGridLayout *SIDFGridLayout=new QGridLayout(SIDF);
    SIDFGridLayout->addWidget(SIDFSlider,0,0,1,2);
    SIDFGridLayout->addWidget(SIDFilterMaxValueSpinBox,0,2,1,1);
    SIDFGridLayout->addWidget(SIDFLabel,0,3,1,1);
    SIDFGridLayout->addWidget(SIDFCheckBox,1,0,1,4);
    SIDFGridLayout->addWidget(minSIDFRangeLabel[0],2,0,1,1);
    SIDFGridLayout->addWidget(minSIDFRangeLabel[1],2,1,1,1);
    SIDFGridLayout->addWidget(minSIDFRangeSlider,2,2,1,2);
    SIDFGridLayout->addWidget(maxSIDFRangeLabel[0],3,0,1,1);
    SIDFGridLayout->addWidget(maxSIDFRangeLabel[1],3,1,1,1);
    SIDFGridLayout->addWidget(maxSIDFRangeSlider,3,2,1,2);
}

//  Baseline surviving Filter functions
void previewSpectraClass::viewBSFilterRangeOnGraph(bool status)
{
    if(status)
    {
        QPen BSFPen(QColor(0,0,0));
        QBrush BSFBrush(QColor(0,0,0,15));
        BSFPen.setStyle(Qt::DashLine);
        BSFPen.setWidth(1);

        QVector<double> xBSFData(2),yBSFData(2);
        xBSFData[0]=xList[0];                       yBSFData[0]=BSFSlider->value()/1000.0;
        xBSFData[1]=xList[noReadingInList-1];       yBSFData[1]=BSFSlider->value()/1000.0;

        previewSpectraPlotObj->graph(BSFilterGraphNumber)->removeFromLegend();
        previewSpectraPlotObj->graph(BSFilterGraphNumber)->setData(xBSFData,yBSFData);
        previewSpectraPlotObj->graph(BSFilterGraphNumber)->setPen(BSFPen);
        previewSpectraPlotObj->graph(BSFilterGraphNumber)->setBrush(BSFBrush);
        previewSpectraPlotObj->graph(BSFilterGraphNumber)->setVisible(true);
        previewSpectraPlotObj->replot();
    }
    else
    {
      previewSpectraPlotObj->graph(BSFilterGraphNumber)->setVisible(false);
      previewSpectraPlotObj->replot();
    }
}
void previewSpectraClass::BSFUpdateFilterBaselineValue()
{
    QVector<double> xBSFData(2),yBSFData(2);
    xBSFData[0]=xList[0];                       yBSFData[0]=BSFSlider->value()/1000.0;
    xBSFData[1]=xList[noReadingInList-1];       yBSFData[1]=BSFSlider->value()/1000.0;


    // display the values
    BSFLabel->setText(QString::number(BSFSlider->value()/1000.0));

    if(!BSFCheckBox->isChecked()) // if not checked ignore the following
        return;

    previewSpectraPlotObj->graph(BSFilterGraphNumber)->setData(xBSFData,yBSFData);
    previewSpectraPlotObj->replot();
}
void previewSpectraClass::applyBSFilterToSpectra()
{
    isSurvivesBSFilterList.clear();
    for(int i=0;i<numGraphsLoaded;i++)
    {
        bool isPass=false;
        for(int j=0;j<noReadingInList;j++)
        {
            if(yList[i][j] > BSFSlider->value()/1000.0)
            {
                isPass=true;
            }
        }
        isSurvivesBSFilterList.append(isPass);
    }
    updateBSFilterDisplayInformation();
}
void previewSpectraClass::updateBSFilterDisplayInformation()
{
    if(!isFilterChoosen)
        return;
    BSFilterLabel->setText("Baseline Surviving Filter: Active");
    currentGraphBSFilterValueLabel->setText("Distance from Reference: NA");
    if(isSurvivesBSFilterList[spectraSlider->value()]==true)
    {
        currentGraphBSFilterLabel->setText("Active Spectrum Survives: Yes");
    }
    else
    {
        currentGraphBSFilterLabel->setText("Active Spectrum Survives: No");
    }

    if(spectraSlider->isEnabled()==false)
    {
        currentGraphBSFilterLabel->setText("Active Spectrum Survives: NA");
    }
    percentGraphsBSSurvivors->setText("Total Surviving Spectra: "+QString::number(isSurvivesBSFilterList.count(true))+"/"+QString::number(isSurvivesBSFilterList.size()));
}
//~ Baseline surviving Filter functions

//Distance Based filter functions
void previewSpectraClass::updateMaxDBFilterSliderValue(double value)
{
    DBFilterMaxValue=value;
    updateDBFilterThresholdAndInformation();
    applyDBFilterToSpectra();
}
void previewSpectraClass::viewDBFilterRangeOnGraph(bool status)
{
    if(status)
    {
        QPen DBFPen(QColor(0,0,0));
        QBrush DBFBrush(QColor(255,255,0,20));
        DBFPen.setStyle(Qt::DashLine);
        DBFPen.setWidth(1);

        QVector<double> xDBFData(4),yDBFData(4);
        xDBFData[0]=xList[minDBFRangeSlider->value()];      yDBFData[0]=1.0;
        xDBFData[1]=xList[minDBFRangeSlider->value()];      yDBFData[1]=0.0;
        xDBFData[2]=xList[maxDBFRangeSlider->value()];      yDBFData[2]=0.0;
        xDBFData[3]=xList[maxDBFRangeSlider->value()];      yDBFData[3]=1.0;

        previewSpectraPlotObj->graph(DBFilterGraphNumber)->removeFromLegend();
        previewSpectraPlotObj->graph(DBFilterGraphNumber)->setData(xDBFData,yDBFData);
        previewSpectraPlotObj->graph(DBFilterGraphNumber)->setPen(DBFPen);
        previewSpectraPlotObj->graph(DBFilterGraphNumber)->setBrush(DBFBrush);
        previewSpectraPlotObj->graph(DBFilterGraphNumber)->setVisible(true);
        previewSpectraPlotObj->replot();
    }
    else
    {
        previewSpectraPlotObj->graph(DBFilterGraphNumber)->setVisible(false);
        previewSpectraPlotObj->replot();
    }
}
void previewSpectraClass::applyDBFilterToSpectra()
{
    if(!isFilterChoosen)
        return;

    EuclideanDistanceList.clear();
    isSurvivesDBFilterList.clear();

    for(int i=0;i<numGraphsLoaded;i++)
    {
        EuclideanDistanceList.append(0);
        for(int j=minDBFRangeFilter;j<maxDBFRangeFilter+1;j++)
        {
            EuclideanDistanceList[i] += qPow(yFilter[j]-yList[i][j],2.0);
        }

        if(EuclideanDistanceList[i] >= DBFSlider->value()/1000.0 * DBFilterMaxValue)
        {
            isSurvivesDBFilterList.append(true);
        }
        else
        {
            isSurvivesDBFilterList.append(false);
        }
    }

    updateDBFilterDisplayInformation();
}
void previewSpectraClass::DBFUpdateFilterRange()
{
    minDBFRangeSlider->setMaximum(maxDBFRangeSlider->value());
    maxDBFRangeSlider->setMinimum(minDBFRangeSlider->value());
    minDBFRangeFilter=minDBFRangeSlider->value();
    maxDBFRangeFilter=maxDBFRangeSlider->value();

    QVector<double> xDBFData(4),yDBFData(4);
    xDBFData[0]=xList[minDBFRangeSlider->value()];      yDBFData[0]=1.0;
    xDBFData[1]=xList[minDBFRangeSlider->value()];      yDBFData[1]=0.0;
    xDBFData[2]=xList[maxDBFRangeSlider->value()];      yDBFData[2]=0.0;
    xDBFData[3]=xList[maxDBFRangeSlider->value()];      yDBFData[3]=1.0;

    // display the values
    minDBFRangeLabel[1]->setText(QString::number(xDBFData[0])+" nm");
    maxDBFRangeLabel[1]->setText(QString::number(xDBFData[3])+" nm");

    if(!DBFCheckBox->isChecked()) // if not checked ignore the following
        return;

    previewSpectraPlotObj->graph(DBFilterGraphNumber)->setData(xDBFData,yDBFData);
    previewSpectraPlotObj->replot();
}
void previewSpectraClass::updateDBFilterDisplayInformation()
{
    if(isFilterChoosen)
    {
        DBFilterLabel->setText("Distance-Based Filter: Active");
    }
    else
    {
        return;
    }

    if (spectraSlider->isEnabled()==true )
    {
        currentGraphDBFilterValueLabel->setText("Euclidean Distance from Ref: "+QString::number(EuclideanDistanceList[spectraSlider->value()]));
    }
    else
    {
        currentGraphDBFilterValueLabel->setText("Euclidean Distance from Ref: NA");
    }

    if(isSurvivesDBFilterList[spectraSlider->value()]==true)
    {
        currentGraphDBFilterLabel->setText("Active Spectrum Survives: Yes");
    }
    else
    {
        currentGraphDBFilterLabel->setText("Active Spectrum Survives: No");
    }

    if (spectraSlider->isEnabled()==false )
    {
        currentGraphDBFilterLabel->setText("Active Spectrum Survives: NA");
    }

    percentGraphsDBSurvivors->setText("Total Surviving Spectra: "+QString::number(isSurvivesDBFilterList.count(true))+"/"+QString::number(isSurvivesDBFilterList.size()));
}
void previewSpectraClass::updateDBFilterThresholdAndInformation()
{
    QString DBFilterInstantValue = QString::number(DBFSlider->value()/1000.0*DBFilterMaxValue);
    DBFilterInstantValue.truncate(5);
    DBFLabel->setText(DBFilterInstantValue);

    if(!isFilterChoosen)
        return;

    currentGraphDBFilterValueLabel->setText("Euclidean Distance from Ref: "+QString::number(EuclideanDistanceList[spectraSlider->value()]));

    for(int i=0;i<numGraphsLoaded;i++)
    {
        if(EuclideanDistanceList[i] >= DBFSlider->value()/1000.0)
        {
            isSurvivesDBFilterList[i]=true;

        }
        else
        {
            isSurvivesDBFilterList[i]=false;
        }
    }

    if(isSurvivesDBFilterList[spectraSlider->value()]==true)
    {
        currentGraphDBFilterLabel->setText("Active Spectrum Survives: Yes");
    }
    else
    {
        currentGraphDBFilterLabel->setText("Active Spectrum Survives: No");
    }

    if (spectraSlider->isEnabled()==false )
    {
        currentGraphDBFilterLabel->setText("Active Spectrum Survives: NA");
    }


    percentGraphsDBSurvivors->setText("Total Surviving Spectra: "+QString::number(isSurvivesDBFilterList.count(true))+"/"+QString::number(isSurvivesDBFilterList.size()));
}
//~Distance Based filter functions

//Ortho-Projection filter functions
void previewSpectraClass::updateMaxOPFilterSliderValue(double value)
{
    OPFilterMaxValue=value;
    updateOPFilterSurvivors();
}
void previewSpectraClass::viewOPFilterRangeOnGraph(bool status)
{
    if(status)
    {
        QPen OPFPen(QColor(0,0,0));
        QBrush OPFBrush(QColor(0,255,0,20));
        OPFPen.setStyle(Qt::DashLine);
        OPFPen.setWidth(1);

        QVector<double> xOPFData(4),yOPFData(4);
        xOPFData[0]=xList[minOPFRangeSlider->value()];      yOPFData[0]=1.0;
        xOPFData[1]=xList[minOPFRangeSlider->value()];      yOPFData[1]=0.0;
        xOPFData[2]=xList[maxOPFRangeSlider->value()];      yOPFData[2]=0.0;
        xOPFData[3]=xList[maxOPFRangeSlider->value()];      yOPFData[3]=1.0;


        previewSpectraPlotObj->graph(OPFilterGraphNumber)->removeFromLegend();
        previewSpectraPlotObj->graph(OPFilterGraphNumber)->setData(xOPFData,yOPFData);
        previewSpectraPlotObj->graph(OPFilterGraphNumber)->setPen(OPFPen);
        previewSpectraPlotObj->graph(OPFilterGraphNumber)->setBrush(OPFBrush);
        previewSpectraPlotObj->graph(OPFilterGraphNumber)->setVisible(true);
        previewSpectraPlotObj->replot();
    }
    else
    {
        previewSpectraPlotObj->graph(OPFilterGraphNumber)->setVisible(false);
        previewSpectraPlotObj->replot();
    }
}
void previewSpectraClass::applyOPFilterToSpectra()
{
    if(!isFilterChoosen)
        return;

    SAMOPList.clear();
    isSurvivesOPFilterList.clear();

    for(int i=0;i<numGraphsLoaded;i++)
    {
        SAMOPList.append(0);
        float currentSpect=0,refSpect=0,currentDotRefSpect=0;
        for(int j=minOPFRangeFilter;j<maxOPFRangeFilter+1;j++)
        {
            currentSpect += pow(yList[i][j],2.0);
            refSpect += pow(yFilter[j],2.0);
            currentDotRefSpect += yList[i][j]*yFilter[j];
        }
        SAMOPList[i]=acos(currentDotRefSpect/(sqrt(currentSpect)*sqrt(refSpect)));
        if(SAMOPList[i] >= OPFSlider->value()/1000.0 * OPFilterMaxValue)
        {
            isSurvivesOPFilterList.append(true);
        }
        else
        {
            isSurvivesOPFilterList.append(false);
        }
    }

    updateOPFilterDisplayInformation();
}
void previewSpectraClass::OPFUpdateFilterRange()
{
    minOPFRangeSlider->setMaximum(maxOPFRangeSlider->value());
    maxOPFRangeSlider->setMinimum(minOPFRangeSlider->value());
    minOPFRangeFilter=minOPFRangeSlider->value();
    maxOPFRangeFilter=maxOPFRangeSlider->value();

    QVector<double> xOPFData(4),yOPFData(4);
    xOPFData[0]=xList[minOPFRangeSlider->value()];      yOPFData[0]=1.0;
    xOPFData[1]=xList[minOPFRangeSlider->value()];      yOPFData[1]=0.0;
    xOPFData[2]=xList[maxOPFRangeSlider->value()];      yOPFData[2]=0.0;
    xOPFData[3]=xList[maxOPFRangeSlider->value()];      yOPFData[3]=1.0;

    // display the values
    minOPFRangeLabel[1]->setText(QString::number(xOPFData[0])+" nm");
    maxOPFRangeLabel[1]->setText(QString::number(xOPFData[3])+" nm");

    if(!OPFCheckBox->isChecked()) // if not checked ignore the following
        return;

    previewSpectraPlotObj->graph(OPFilterGraphNumber)->setData(xOPFData,yOPFData);
    previewSpectraPlotObj->replot();
}
void previewSpectraClass::updateOPFilterSurvivors()
{
    if(!isFilterChoosen)
        return;

    for(int i=0;i<numGraphsLoaded;i++)
    {
        if(SAMOPList[i] >= OPFSlider->value()/1000.0 * OPFilterMaxValue)
        {
            isSurvivesOPFilterList[i]=true;
        }
        else
        {
            isSurvivesOPFilterList[i]=false;
        }
    }

    updateOPFilterDisplayInformation();
}
void previewSpectraClass::updateOPFilterDisplayInformation()
{
    QString OPFilterInstantValue = QString::number(OPFSlider->value()/1000.0*OPFilterMaxValue);
    OPFilterInstantValue.truncate(5);
    OPFLabel->setText(OPFilterInstantValue);

    if(isFilterChoosen)
    {
        OPFilterLabel->setText("Ortho.-Proj. Based Filter: Active");
    }
    else
    {
        return;
    }

    if(spectraSlider->isEnabled())
    {
        currentGraphOPFilterValueLabel->setText("Divergence from Reference: "+QString::number(SAMOPList[spectraSlider->value()]));
    }
    else
    {
        currentGraphOPFilterValueLabel->setText("Divergence from Reference: NA");
    }

    if(isSurvivesOPFilterList[spectraSlider->value()]==true)
    {
        currentGraphOPFilterLabel->setText("Active Spectrum Survives: Yes");
    }
    else
    {
        currentGraphOPFilterLabel->setText("Active Spectrum Survives: No");
    }

    if (spectraSlider->isEnabled()==false )
    {
        currentGraphOPFilterLabel->setText("Active Spectrum Survives: NA");
    }
    percentGraphsOPSurvivors->setText("Total Surviving Spectra: "+QString::number(isSurvivesOPFilterList.count(true))+"/"+QString::number(isSurvivesOPFilterList.size()));
}
//~Ortho-Projection filter functions

//Spectral information Divergence filter functions
void previewSpectraClass::updateMaxSIDFilterSliderValue(double value)
{
    SIDFilterMaxValue=value;
    updateSIDFilterSurvivors();
}
void previewSpectraClass::viewSIDFilterRangeOnGraph(bool status)
{
    if(status)
    {
        QPen SIDFPen(QColor(0,0,0));
        QBrush SIDFBrush(QColor(0,0,255,20));
        SIDFPen.setStyle(Qt::DashLine);
        SIDFPen.setWidth(1);

        QVector<double> xSIDFData(4),ySIDFData(4);
        xSIDFData[0]=xList[minSIDFRangeSlider->value()];      ySIDFData[0]=1.0;
        xSIDFData[1]=xList[minSIDFRangeSlider->value()];      ySIDFData[1]=0.0;
        xSIDFData[2]=xList[maxSIDFRangeSlider->value()];      ySIDFData[2]=0.0;
        xSIDFData[3]=xList[maxSIDFRangeSlider->value()];      ySIDFData[3]=1.0;

        previewSpectraPlotObj->graph(SIDFilterGraphNumber)->removeFromLegend();
        previewSpectraPlotObj->graph(SIDFilterGraphNumber)->setData(xSIDFData,ySIDFData);
        previewSpectraPlotObj->graph(SIDFilterGraphNumber)->setPen(SIDFPen);
        previewSpectraPlotObj->graph(SIDFilterGraphNumber)->setBrush(SIDFBrush);
        previewSpectraPlotObj->graph(SIDFilterGraphNumber)->setVisible(true);
        previewSpectraPlotObj->replot();
    }
    else
    {
        previewSpectraPlotObj->graph(SIDFilterGraphNumber)->setVisible(false);
        previewSpectraPlotObj->replot();
    }
}
void previewSpectraClass::applySIDFilterToSpectra()
{
    if(!isFilterChoosen)
        return;

    discrepanciesSID.clear();
    isSurvivesSIDFilterList.clear();
    QList<double> qFilterProbList,pDataProbList;
    double qFilterSum=0.0,pDataSum=0.0;

    for(int i=0;i<numGraphsLoaded;i++)
    {
        pDataProbList.clear();
        pDataSum=0.0;

        for(int j=minSIDFRangeFilter;j<maxSIDFRangeFilter+1;j++)
        {
            if(i == 0)
                qFilterSum +=yFilter[j];
            pDataSum +=yList[i][j];
        }

        for(int j=minSIDFRangeFilter;j<maxSIDFRangeFilter+1;j++)
        {
            if(i == 0)
                qFilterProbList.append(yFilter[j]/qFilterSum);
            pDataProbList.append(yList[i][j]/pDataSum);
        }

        discrepanciesSID.append(0);
        for(int j=minSIDFRangeFilter;j<maxSIDFRangeFilter+1;j++)
        {
            discrepanciesSID[i] += pDataProbList[j-minSIDFRangeFilter] * log(pDataProbList[j-minSIDFRangeFilter]/qFilterProbList[j-minSIDFRangeFilter]);
        }
        if(discrepanciesSID[i] >= SIDFSlider->value()/1000.0 * SIDFilterMaxValue)
        {
            isSurvivesSIDFilterList.append(true);
        }
        else
        {
            isSurvivesSIDFilterList.append(false);
        }
    }

    updateSIDFilterSurvivors();
}
void previewSpectraClass::SIDFUpdateFilterRange()
{
    minSIDFRangeSlider->setMaximum(maxSIDFRangeSlider->value());
    maxSIDFRangeSlider->setMinimum(minSIDFRangeSlider->value());
    minSIDFRangeFilter=minSIDFRangeSlider->value();
    maxSIDFRangeFilter=maxSIDFRangeSlider->value();

    QVector<double> xSIDFData(4),ySIDFData(4);
    xSIDFData[0]=xList[minSIDFRangeSlider->value()];      ySIDFData[0]=1.0;
    xSIDFData[1]=xList[minSIDFRangeSlider->value()];      ySIDFData[1]=0.0;
    xSIDFData[2]=xList[maxSIDFRangeSlider->value()];      ySIDFData[2]=0.0;
    xSIDFData[3]=xList[maxSIDFRangeSlider->value()];      ySIDFData[3]=1.0;

    // display the values
    minSIDFRangeLabel[1]->setText(QString::number(xSIDFData[0])+" nm");
    maxSIDFRangeLabel[1]->setText(QString::number(xSIDFData[3])+" nm");

    if(!SIDFCheckBox->isChecked()) // if not checked ignore the following
        return;

    previewSpectraPlotObj->graph(SIDFilterGraphNumber)->setData(xSIDFData,ySIDFData);
    previewSpectraPlotObj->replot();
}
void previewSpectraClass::updateSIDFilterSurvivors()
{
    if(!isFilterChoosen)
        return;

    for(int i=0;i<numGraphsLoaded;i++)
    {
        if(discrepanciesSID[i] >= SIDFSlider->value()/1000.0 * SIDFilterMaxValue)
        {
            isSurvivesSIDFilterList[i]=true;
        }
        else
        {
            isSurvivesSIDFilterList[i]=false;
        }
    }

    updateSIDFilterDisplayInformation();
}
void previewSpectraClass::updateSIDFilterDisplayInformation()
{
    QString SIDFilterInstantValue = QString::number(SIDFSlider->value()/1000.0*SIDFilterMaxValue);
    SIDFilterInstantValue.truncate(5);
    SIDFLabel->setText(SIDFilterInstantValue);

    if(isFilterChoosen)
    {
        SIDFilterLabel->setText("Info.-Div. Based Filter: Active");
    }
    else
    {
        return;
    }

    if(spectraSlider->isEnabled())
    {
        currentGraphSIDFilterValueLabel->setText("Discrepancy wrt Reference: "+QString::number(discrepanciesSID[spectraSlider->value()]));
    }
    else
    {
        currentGraphSIDFilterValueLabel->setText("Discrepancy wrt Reference: NA");
    }

    if(isSurvivesSIDFilterList[spectraSlider->value()]==true )
    {
        currentGraphSIDFilterLabel->setText("Active Spectrum Survives: Yes");
    }
    else
    {
        currentGraphSIDFilterLabel->setText("Active Spectrum Survives: No");
    }

    if (spectraSlider->isEnabled()==false)
    {
        currentGraphSIDFilterLabel->setText("Active Spectrum Survives: NA");
    }
    percentGraphsSIDSurvivors->setText("Number of Survivors: "+QString::number(isSurvivesSIDFilterList.count(true))+"/"+QString::number(isSurvivesSIDFilterList.size()));
}
//~Spectral information Divergence filter functions

void previewSpectraClass::filtersBrowser()
{
    QPen activePen(QColor(34,139,34,255));
    activePen.setStyle(Qt::SolidLine);
    activePen.setWidth(1.5);

    QPen plotsPen(QColor(255,0,0,255));
    plotsPen.setStyle(Qt::SolidLine);
    plotsPen.setWidth(1);

    if(isShowAllGraphsButtonClicked) // If all graphs are showed
    {
        previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->setPen(plotsPen);
        previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->removeFromLegend();

        previewSpectraPlotObj->graph(filtersSlider->value())->setPen(activePen);
        previewSpectraPlotObj->graph(filtersSlider->value())->setName("Ref. Spectrum");
        previewSpectraPlotObj->graph(filtersSlider->value())->addToLegend();
    }
    else // If sole graph is showed
    {
        previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->setPen(plotsPen);
        previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->removeFromLegend();
        previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->setVisible(false);

        previewSpectraPlotObj->graph(spectraSlidersLastValue)->setPen(plotsPen);
        previewSpectraPlotObj->graph(spectraSlidersLastValue)->setName("Data Spectrum");
        previewSpectraPlotObj->graph(spectraSlidersLastValue)->addToLegend();
        previewSpectraPlotObj->graph(spectraSlidersLastValue)->setVisible(true);

        previewSpectraPlotObj->graph(filtersSlider->value())->setPen(activePen);
        previewSpectraPlotObj->graph(filtersSlider->value())->setName("Ref. Spectrum");
        previewSpectraPlotObj->graph(filtersSlider->value())->addToLegend();
        previewSpectraPlotObj->graph(filtersSlider->value())->setVisible(true);
    }
    previewSpectraPlotObj->replot();
    filtersSliderSlidersLastValue=filtersSlider->value();
}
void previewSpectraClass::showAllorSoleSpectra()
{
    isShowAllGraphsButtonClicked=!isShowAllGraphsButtonClicked;
    if(isShowAllGraphsButtonClicked) // show all graphs is active
    {
        spectraSlider->setDisabled(true);
        showAllGraphsButton->setText("Show Sole Spectra");

        for(int i=0;i<numGraphsLoaded;i++)
        {
            previewSpectraPlotObj->graph(i)->setVisible(true);
        }
        previewSpectraPlotObj->replot();
    }
    else // show one graph per time is active
    {
        spectraSlider->setDisabled(false);
        showAllGraphsButton->setText("Show ALL Spectra");

        for(int i=0;i<numGraphsLoaded;i++)
        {
            previewSpectraPlotObj->graph(i)->setVisible(false);
        }
        previewSpectraPlotObj->graph(spectraSlider->value())->setVisible(true);
        previewSpectraPlotObj->replot();
    }

    updateBSFilterDisplayInformation();
    updateDBFilterDisplayInformation();
    updateOPFilterDisplayInformation();
    updateSIDFilterDisplayInformation();
}
void previewSpectraClass::spectraBrowser()
{
    QPen activePen(QColor(34,139,34,255));
    activePen.setStyle(Qt::SolidLine);
    activePen.setWidth(1);

    QPen plotsPen(QColor(255,0,0,255));
    plotsPen.setStyle(Qt::SolidLine);
    plotsPen.setWidth(1);

    previewSpectraPlotObj->graph(spectraSlidersLastValue)->setVisible(false);
    previewSpectraPlotObj->graph(spectraSlidersLastValue)->removeFromLegend();


    previewSpectraPlotObj->graph(spectraSlider->value())->setVisible(true);
    previewSpectraPlotObj->graph(spectraSlider->value())->setName("Data Spectrum");
    previewSpectraPlotObj->graph(spectraSlider->value())->addToLegend();

    previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->setPen(activePen);
    previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->setName("Ref. Spectrum");
    previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->addToLegend();
    previewSpectraPlotObj->graph(filtersSliderSlidersLastValue)->setVisible(true);

    previewSpectraPlotObj->replot();
    spectraSlidersLastValue=spectraSlider->value();

    applyBSFilterToSpectra();
    updateDBFilterThresholdAndInformation();
    updateOPFilterSurvivors();
    updateSIDFilterSurvivors();
}
void previewSpectraClass::setSpectrumAsFilter()
{
    xFilter.clear();
    yFilter.clear();

    xFilter.operator =(xList);
    yFilter.operator =(yList[filtersSliderSlidersLastValue]);
    isFilterChoosen=true;

    // set icon to cheched as set
    setRefFilterButton->setIcon(QIcon(":/icons/resources/done.png"));
    loadRefSpectraButton->setIcon(QIcon(":/icons/resources/notDone.png"));
    applyBSFilterToSpectra();
    applyDBFilterToSpectra();
    applyOPFilterToSpectra();
    applySIDFilterToSpectra();

    // set filter to spectrum
    isFilterLoaded=false;
    filtersSlider->setMaximum(numGraphsLoaded-1);
    QPen filterPen(QColor(0,255,0,255));
    filterPen.setStyle(Qt::SolidLine);
    filterPen.setWidth(1);

    QVector<double> xData=QVector<double>::fromList(xFilter);
    QVector<double> yData=QVector<double>::fromList(yFilter);
    previewSpectraPlotObj->graph(numGraphsLoaded-1)->setData(xData,yData);
    previewSpectraPlotObj->graph(numGraphsLoaded-1)->setPen(filterPen);
    previewSpectraPlotObj->replot();
}
void previewSpectraClass::clearChoosedFilterFunction()
{
    // change the icons of buttons that set and load filters
    setRefFilterButton->setIcon(QIcon(":/icons/resources/notDone.png"));
    //loadRefFilterButton->setIcon(QIcon(":/icons/resources/notDone.png"));
}
void previewSpectraClass::exportRefSpectrum()
{
    QString refFileName=QFileDialog::getSaveFileName(this,"Save to File","./untitled.dat",tr("files(*.dat )"));
    QFile refFile(refFileName);
       if (!refFile.exists())
           refFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream refFileStream(&refFile);

    while(refFile.isOpen())
    {
        refFileStream << "Wavelength:"<<"\t";
        for(int i=0;i<noReadingInList;i++)
        {
            refFileStream <<xFilter[i]<<"\t";
        }
        refFileStream << "\n";

        refFileStream << "Intensity:"<<"\t";
        for(int i=0;i<noReadingInList;i++)
        {
            refFileStream <<yFilter[i]<<"\t";
        }
        break;
    }
}
void previewSpectraClass::loadRefSpectrum()
{
    QString refFileName=QFileDialog::getOpenFileName(this,"Open File",QApplication::applicationDirPath(),tr("files(*.txt *.ref *.dat )"));
    QFile refFile(refFileName);
    if (!refFile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

    QTextStream inData(&refFile);

    xFilter.clear();
    yFilter.clear();

    while(!inData.atEnd())
    {
        QString currentLine = inData.readLine();
        noReadingInFilterList=currentLine.count("\t")-1;

        if(currentLine.startsWith("Wavelength:"))
        {
            for (int i=0; i<noReadingInFilterList; i++)
            {
                xFilter << currentLine.section("\t",i+1,i+1).toFloat();
            }
        }
        else if(currentLine.startsWith("Intensity:"))
        {
            for (int i=0; i<noReadingInFilterList; i++)
            {
                yFilter << currentLine.section("\t",i+1,i+1).toFloat();
            }
        }
    }

    if(xFilter.size()!=yFilter.size())
    {
        QMessageBox::information(this, tr("Information"),tr("Error Loading File. wavelength elements does not equal intensity elements."));
        return;
    }
    else if (xFilter.size()==0)
    {
        QMessageBox::information(this, tr("Information"),tr("Error Loading File. Wavelenth vector is empty."));
        return;
    }
    else if (yFilter.size()==0)
    {
        QMessageBox::information(this, tr("Information"),tr("Error Loading File. Intensity vector is empty."));
        return;
    }

    isFilterChoosen=true;
    isFilterLoaded=true;

    filtersSlider->setMaximum(numGraphsLoaded);
    filtersSlider->setValue(numGraphsLoaded);
    QPen filterPen(QColor(0,255,0,255));
    filterPen.setStyle(Qt::SolidLine);
    filterPen.setWidth(1);

    QVector<double> xData=QVector<double>::fromList(xFilter);
    QVector<double> yData=QVector<double>::fromList(yFilter);
    previewSpectraPlotObj->graph(numGraphsLoaded)->setData(xData,yData);
    previewSpectraPlotObj->graph(numGraphsLoaded)->setPen(filterPen);
    previewSpectraPlotObj->replot();

    setRefFilterButton->setIcon(QIcon(":/icons/resources/notDone.png"));
    loadRefSpectraButton->setIcon(QIcon(":/icons/resources/done.png"));

    applyBSFilterToSpectra();
    applyDBFilterToSpectra();
    applyOPFilterToSpectra();
    applySIDFilterToSpectra();
}
void previewSpectraClass::exportFilterSettings()
{

}
void previewSpectraClass::exportSurvivedSpectrum()
{

    QMessageBox::information(this, tr("Information"),tr("Filters applied are the checked on the right panel only."));

    QFile spectraFile(exportFileName);
    if (!spectraFile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

    QString exportFileNameTMP=exportFileName;
    exportFileNameTMP.chop(4);
    exportFileNameTMP.append("_filtered.dat");
    QString exportDataFileName=QFileDialog::getSaveFileName(this,"Save to File",exportFileNameTMP,tr("files(*.dat )"));
    QFile exportDataFile(exportDataFileName);
       if (!exportDataFile.open(QIODevice::ReadWrite | QIODevice::Text))
        return;

       QProgressDialog waitMSDialog("Exporting Graphs ...", "Abort", 0, numGraphsLoaded, this);
       waitMSDialog.setWindowModality(Qt::WindowModal);
       waitMSDialog.setWindowTitle("Status");

    QTextStream inData(&spectraFile);
    QTextStream outData(&exportDataFile);

    QRegExp dataPattern("\\d\\d\\d\\d-\\d\\d-\\d\\d");

    int graphIndex=0;
    QString currentLine,currentFile;
    while(!inData.atEnd())
    {
        currentLine = inData.readLine();

        if(currentLine.startsWith("#Date"))
        {
            currentFile.append(currentLine+"\n");
        }
        else if (dataPattern.exactMatch(currentLine.section("\t",0,0)))
        {
            bool isPassed=true;
            if(BSF->isChecked() and isSurvivesBSFilterList[graphIndex]==false)
            {
                isPassed=false;
            }
            if(DBF->isChecked() and isSurvivesDBFilterList[graphIndex]==false)
            {
                isPassed=false;
            }
            if(OPF->isChecked() and isSurvivesOPFilterList[graphIndex]==false)
            {
                isPassed=false;
            }
            if(SIDF->isChecked() and isSurvivesSIDFilterList[graphIndex]==false)
            {
                isPassed=false;
            }
            if(isPassed)
            {
                currentFile.append(currentLine+"\n");
            }
            waitMSDialog.setValue(graphIndex);
            if(waitMSDialog.wasCanceled())
                break;
            graphIndex++;
        }
    }
    waitMSDialog.setValue(graphIndex);

    exportDataFile.resize(0);
    outData << currentFile;
    exportDataFile.close();
    spectraFile.close();
    waitMSDialog.close();

}
