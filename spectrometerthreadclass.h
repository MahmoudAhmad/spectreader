#ifndef SPECTROMETERTHREADCLASS_H
#define SPECTROMETERTHREADCLASS_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QTime>

class spectrometerThreadClass : public QThread
{
    QTimer  *timer;
public:
    explicit spectrometerThreadClass(QObject *parent = 0);
    ~spectrometerThreadClass();
    void run();

};

#endif // SPECTROMETERTHREADCLASS_H
