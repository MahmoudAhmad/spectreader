/*
 Author: Mahmoud Ahmad
 Date: 5-12-2015
 copywrite: ----
*/

#include "mainwindow.h"
#include <QApplication>
#include <QtCore>



int main(int argc, char *argv[])
{
    QApplication PDTSpectsQApp(argc, argv);

    MainWindow PDTSpectsMainWindow;

    QFont defaultFont("Meiryo UI", 9);
    PDTSpectsMainWindow.setFont(defaultFont);
    PDTSpectsMainWindow.setWindowTitle("PENN PDT LAB: Spectrometers controller");

    PDTSpectsMainWindow.setFixedSize(1350,690);
    PDTSpectsMainWindow.show();

    return PDTSpectsQApp.exec();
}
