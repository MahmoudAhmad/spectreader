#ifndef APPLYNEWPARAMETERSTIMERCLASS_H
#define APPLYNEWPARAMETERSTIMERCLASS_H
#include <QTimer>
#include <QObject>

class applyNewParametersTimerClass : public QObject
{
    Q_OBJECT
public:
    explicit applyNewParametersTimerClass(QObject *parent = 0);
    ~applyNewParametersTimerClass();
    QTimer *timerObj;


signals:

public slots:


};

#endif // APPLYNEWPARAMETERSTIMERCLASS_H
