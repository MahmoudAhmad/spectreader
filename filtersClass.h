#ifndef FILTERSCLASS_H
#define FILTERSCLASS_H
#include <QWidget>
#include <QCheckBox>
#include <QGridLayout>
#include "toolBarclass.h"

QT_BEGIN_NAMESPACE
namespace uiNameSpace {

extern QCustomPlot *plotArea;

extern bool isBackgroundAcquired;
extern bool isBackgroundSubtracted;
extern bool isBackgroundNeedToBeSubtracted;
extern toolBarClass *mainToolBarObj;
extern QFont WinXFontS,WinXFontM,WinXFontL; // fonts changes with operating systems.

} // namespace uiNameSpace

namespace filters {
extern bool isThresholdFilterEnabled;
extern int thresholdFilterPercentofPixels,thresholdFilterBaseLine;

extern bool isDistanceBasedFilterEnabled;

}


QT_END_NAMESPACE


class filtersClass : public QWidget
{
    Q_OBJECT
    QCheckBox *subtractBackgroundCheckBox;
    QGridLayout *filtersOuterGridLayout,*filtersInnerGridLayout;

private slots:
    void setSubtractBackgroundStatus(bool);

public:
    explicit filtersClass(QWidget *parent = 0);
    ~filtersClass();

signals:
    void setRunStopButtonStatus(bool);

private:
    void makeThresholdFilter();
    void makeDistanceBasedFilter();
    void makeOrthogonalProjectionFilter();
    void makeSpectralInformationDivergenceFilter();

private slots:
    void enableThresholdFilter(bool);
    void setThresholdFiltersNoPixels(int);
    void setThresholdBaseLine(int);
};

#endif // FILTERSCLASS_H
