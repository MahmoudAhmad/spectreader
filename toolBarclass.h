#ifndef RUNSTOPCLASS_H
#define RUNSTOPCLASS_H
#include "specthreadclass.h"
#include "plotspectraclass.h"
#include <QProgressBar>
#include <QToolBar>
#include <QToolButton>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace uiNameSpace {

extern QString outputFilesDir;
extern QString spectC_Code[8],preferedSpectsOrder[8];

extern bool openNewFiles[8];

extern bool isAcquiringLightNow;
extern bool acquireLightOnceNow;
extern plotSpectraClass *plotSpectraObj;
extern unsigned short spectBackGroundSignal[8][2048];
extern bool isAcquiringDarkSignal;
extern bool isBackgroundAcquired;
extern QProgressBar *mainProgressBarObj;
extern QStatusBar *mainStatusBarObj;

extern QString settingsFileName;
extern QString settingsFileDir;
extern QSettings* settings;

} // namespace uiNameSpace

QT_END_NAMESPACE


class toolBarClass : public QToolBar
{
    Q_OBJECT

    unsigned long progressCounter=0;
    QMutex progressBarLock;

    QString setDirectoryQString;
    QString openFilesQString;

    QToolButton *setDirectoryButton=new QToolButton;
    QToolButton *openOutputFolderButton=new QToolButton;

    QToolButton *reloadDevicesButton=new QToolButton;
    QToolButton *saveSettingsButton=new QToolButton;
    QToolButton *loadSettingsButton=new QToolButton;


    QToolButton *runStopToolButton=new QToolButton;
    QToolButton *acquireLightOnceButton=new QToolButton;
    QToolButton *acquireBackgroundButton=new QToolButton;
    QToolButton *informationButton=new QToolButton;
    QToolButton *previewOldDataButton=new QToolButton;
    QToolButton *GenerateFiltersButton=new QToolButton;


    QFileDialog *setOutputDirectory,*openOutputFile;
    QLabel *saveSettingsDialogDirLabel;

    specThreadClass *spectThreads[8];

    bool isRunStopButtonClicked=false;
    bool isAcquireDarkSignalClicked=false;
    bool isRunOnceButtonClicked=false;


    bool acquireingStoringPlottingIsRunning();
    void stopAcquiringStoringPlottingThreads();
public:

public:
    explicit toolBarClass(QWidget *parent = 0);
    ~toolBarClass();

signals:
    void updateActiveSpects();
    void exportRunParameters();
    void exportSpectrometerParamters();
    void readRunParameters();
    void readSpectrometerParamters();


private slots:
    void setOutputDirectoryFunction();
    void saveSettingsFunction();
    void loadSettingsFunction();
    void enableDataButtons();
    void previewSpectraFromFile();

public slots:
    bool checkForTimingParametersAndRunIfSucceed();
    bool acquiringDataNow();
    void acquireLightOnce();
    void acquireDarkSignalNow();
    void reInitiateSpectrometers();
    void updateProgress();
    void setRunStopButtonBasedonBackgourndAvailability(bool);
    void openOutputFilesFunction();
    void progressBaris100(int);    
};

#endif // RUNSTOPCLASS_H
