//______________________________________________________________________________
//
// Copyright (c) B&W TEK.INC 2012
// All rights reserved.
//
//______________________________________________________________________________
/*
#ifndef BWTEKUSB
#define BWTEKUSB

#define DLLIMPORT extern "C" __declspec(dllimport)
#define CALLINGMETHOD __stdcall


#pragma pack(push,1)
typedef struct _DATAPARA
{
    int nOptionX;                  
	int nStartX;                   
	int nEndX;                     
	int nIncX;                     
	int nPixelNumber;	
} DataExport_Parameter_Struct, *pData_Export_Parameter_Struct;

typedef struct _DATAPARA0
{
	double fExcitationWavelength;  
	double fCoefsA[4];
    double fCoefsB[4];	
} DataExport_Parameter_Struct0, *pData_Export_Parameter_Struct0;


DLLIMPORT bool CALLINGMETHOD InitDevices();
DLLIMPORT int CALLINGMETHOD GetDeviceCount();
DLLIMPORT int CALLINGMETHOD GetUSBType(int *nUSBType, int nChannel);
DLLIMPORT int CALLINGMETHOD GetCCode (char *pCCode, int nChannel);
DLLIMPORT int CALLINGMETHOD CloseDevices();


DLLIMPORT int CALLINGMETHOD bwtekTestUSB (int nTimingMode, int nPixelNo, int nInputMode, int nChannel, void *pExtraData);
DLLIMPORT int CALLINGMETHOD bwtekSetTimeUSB ( long  lTime,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetTimeBaseUSB ( long lTime,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetTimeBase0USB (long  lTime, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetTimingsUSB (long lTriggerExit,int nMuliply,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekDataReadUSB (int  nTriggerMode, unsigned short *pArray, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekReadResultUSB (int  nTriggerMode, int nAverage, int nTypeSmoothing, int nValueSmoothing, unsigned short *pArray, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekDataReadUSB1 (int  nSpectrumNo, int  nTriggerMode, unsigned short* pArray, double *pStartTime,double *pEndTime,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekCloseUSB (int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekReadEEPROMUSB (char *str, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekStopIntegration (int nChannel);


DLLIMPORT int CALLINGMETHOD bwtekSmoothingUSB (  int nTypeSmoothing, int nValueSmoothing, double* pArray, int nNum);
DLLIMPORT int CALLINGMETHOD bwtekConvertDerivativeDouble (int nSelectMethod, int nPointHalf, int nPolynominalOrder, int nDerivativeOrder, double * pRawArray,double * pResultArray, int nDataNumber);
DLLIMPORT int CALLINGMETHOD bwtekDifferentiateDouble (int nPointInterval, double * pRawArray,double * pWavelengthArray,double * pResultArray, int nDataNumber);
DLLIMPORT int CALLINGMETHOD bwtekPolyFit (double *x, double *y, int numPts,double *coefs, int order);
DLLIMPORT int CALLINGMETHOD bwtekPolyCalc (double *coefs, int order, double x, double *y);
DLLIMPORT int CALLINGMETHOD bwtekDataExport (DataExport_Parameter_Struct *pDataExport_Parameter_Struct,DataExport_Parameter_Struct0 *pDataExport_Parameter_Struct0, double * pRawArray,double * pResultArray,int *nResultArrayLen);

 
DLLIMPORT int CALLINGMETHOD bwtekSetExtLaser (int nOnOff, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetExtShutter (int nOnOff, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetExtSync (int nOnOff, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekGatedMode (int nGatedTime, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetExtPulse (int nOnOff, int nDelay,int nHigh, int nLow, int nPulse,int nInverse,int nChannel );

DLLIMPORT int CALLINGMETHOD bwtekGetExtStatus (int nChannel );
DLLIMPORT int CALLINGMETHOD bwtekGetTTLIn (int nNo, int *nValue, int nChannel );
DLLIMPORT int CALLINGMETHOD bwtekSetTTLOut (int nNo, int nSetValue, int nInverse, int nChannel );
DLLIMPORT int CALLINGMETHOD bwtekGetAnalogIn (int nNo, int *nValue, double *fValue,int nChannel );
DLLIMPORT int CALLINGMETHOD bwtekSetAnalogOut(int nNo, int nDAValue, int nChannel );

DLLIMPORT int CALLINGMETHOD bwtekLEDOn (int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekLEDOff (int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekLEDDelay (unsigned nDelay, int nChannel);

DLLIMPORT int CALLINGMETHOD bwtekSetPulseNo (int nPulseNo,int nChannel );

DLLIMPORT int CALLINGMETHOD bwtekShutterOpen (int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekShutterClose (int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekShutterInverse (int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekShutterControl (int nSetShutter1,int nSetShutter2, int nChannel);

//BTC261E, BTC262E
DLLIMPORT int CALLINGMETHOD bwtekSetABGain (int nAB,int nGain, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetABOffset (int nAB, int nOffset, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekGetABGain (int nAB, int *nGain, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekGetABOffset (int nAB, int *nOffset, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetInGaAsMode (int nMode, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekGetInGaAsMode (int *nMode, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekQueryTemperature (int nCommand, int &nRaw, double &fTemp, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekAccessDeltaTemp (int nOnOff, double *fNum,int nChannel );
DLLIMPORT int CALLINGMETHOD bwtekAccessDeltaTemp1 (int nOnOff, double *fNum,double *fNum1,int nChannel );
DLLIMPORT int CALLINGMETHOD bwtekReadValue (int nItem, int *nGetValue,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekWriteValue (int nItem, int nSetValue,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekGetTimeUSB (long  *lTime,int nChannel);

DLLIMPORT int CALLINGMETHOD bwtekSetTimeUnitUSB (int  nTimeUnit,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekGetTimeUnitUSB (int  *nTimeUnit,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetInGaAsMode (int nMode, int nChannel);




DLLIMPORT int CALLINGMETHOD bwtekSetupChannel (int nFlag,char *nChannelStatus);
DLLIMPORT int CALLINGMETHOD bwtekSaveEEPROMChannel (char *nChannelStatus);
DLLIMPORT int CALLINGMETHOD bwtekGetCCode (char *pCCode, int nChannel)
DLLIMPORT int CALLINGMETHOD bwtekGetXaxisInverseByte (int *nXaxisInverseByte, int nChannel)


// USB3.0 support
DLLIMPORT int CALLINGMETHOD bwtekDSPDataReadUSB (int  nAveNum,int  nSmoothing,int  nDarkCompensate,int  nTriggerMode, unsigned short *pArray, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekFrameDataReadUSB (int  nFrameNum, int  nTriggerMode, unsigned short *pArray, int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekEraseBlockUSB (int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekWriteBlockUSB (unsigned int nAddrress,BYTE* pDataArray, int nNum,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekReadBlockUSB (unsigned int nAddrress,BYTE* pDataArray, int nNum,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekReadTemperature (int ADChannel, int &nRaw, double &fTemp,int nChannel);
DLLIMPORT int CALLINGMETHOD bwtekSetTemperature (int DAChannel, int nSetTemp, int nChannel);

DLLIMPORT int CALLINGMETHOD bwtekSetLowNoiseModeUSB ( int nEanbleLowNoiseMode,int nChannel);



#endif 
*/
