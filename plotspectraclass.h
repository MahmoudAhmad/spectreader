#ifndef PLOTSPECTRACLASS_H
#define PLOTSPECTRACLASS_H
#include <QWidget>
#include "qcustomplot.h"
#include <QSlider>
#include <QList>
#include <QProgressBar>


QT_BEGIN_NAMESPACE

namespace uiNameSpace {

extern int noSpectrometers;

extern QString spectrometerName[8];

extern int const noPixels;
extern int xBeginPixel[8],xEndPixel[8];
extern float wavelengthToPixelFactor[8];
extern float xMinGlobalWavelength,xMaxGlobalWavelength;
extern float beginScanFrequency,endScanFrequency;

extern unsigned short spectOutput[8][2048];
extern QCustomPlot *plotArea;
extern float minWavelength[8],maxWavelength[8];

//data conversion
extern double fcoeffA[8][4];
extern double fcoeffB[8][4];

extern QList<QString> graphsTimeStamp[8];
extern QString oldFileNames[8];

extern QProgressBar *mainProgressBarObj;
extern QFont WinXFontS,WinXFontM,WinXFontL; // fonts changes with operating systems.

extern float yMinValue,yMaxValue;


} // namespace uiNameSpace


QT_END_NAMESPACE


class plotSpectraClass : public QWidget
{
    Q_OBJECT

    QCustomPlot *PlotArea,*solePlots[8];
    QPen pen[8];
    QBrush brush[8];
    QTabWidget *plotsTabWidget;
    QSlider *plotsSlider[8];
    QHBoxLayout *plotsHBoxLayout[8];
    QVBoxLayout *PlotsVBLayout[8];
    QWidget *plotsWidgetforLayouts[8];
    QPushButton *PlotsHideButton[8];
    QPushButton *PlotsDeleteButton[8];
    QLabel *plotsSliderLabel[8];

    double fCoefsA[4];
    double fCoefsB[4];

    QList<int> xMinAxis[8],xMaxAxis[8];

    QFile tmpFileToDeleteData[8];
    QTextStream tmpText[8];
    QString oldLine[8],newLines[8];
    QList<bool> isGraphHidden[8];

    long lastPlotIndex[8]={0,0,0,0,0,0,0,0};

    unsigned long graphIndex[8]={0,0,0,0,0,0,0,0};

public:
    explicit plotSpectraClass(QWidget *parent = 0);
    ~plotSpectraClass();

    void startPlotting();

signals:

public slots:
    void plotRecievedData(int,int);
    void setNewFrequencyRange();
    void setNewIntensityRange();
    void setNewGraphsNames();
    void hideLegend(bool isVisibleLegend);
    void resetPlotParameters(int);

private slots:
    void updateFigsInTabs(int);
    void deleteGraph();
    void hideGraphsInTimeLine();

};

#endif // PLOTSPECTRACLASS_H
