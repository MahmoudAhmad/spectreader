#include "specthreadclass.h"
//#include "bwtekusb.h"
#include <QTime>
#include <QDate>
#include <QDebug>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QCoreApplication>
#include <QMessageBox>
#include <QLibrary>


specThreadClass::specThreadClass()
{
    connect(this,SIGNAL(quitImediately()),this,SLOT(quitThreadNow()));
    connect(this,SIGNAL(resetPlotParameters(int)),uiNameSpace::plotSpectraObj,SLOT(resetPlotParameters(int)));
}

specThreadClass::~specThreadClass()
{

}

void specThreadClass::setSpectrometerIndex(unsigned int currentChannelObj,unsigned int preferenceIndex)
{
  this->spectrometerIndex=preferenceIndex;
  this->currentChannel=currentChannelObj;

}

void specThreadClass::run()
{
    this->timeStamp=QTime::currentTime().toString("hh-mm-ss");
    this->dateStamp=QDate::currentDate().toString("yyyy-MM-dd");

//-------------------------------------------------
    QLibrary specLibrary("BWTEKUSB.DLL");
    if(!specLibrary.isLoaded())
        specLibrary.load();

    typedef int (__stdcall *testSptrometer)(int,int,int,int,QString);
    testSptrometer testSptrometerObj = (testSptrometer) specLibrary.resolve("bwtekTestUSB");
    testSptrometerObj(1,2048,1,currentChannel,NULL);

    typedef long (__stdcall *setExposureTime)(long,int);
    setExposureTime setExposureTimeObj = (setExposureTime) specLibrary.resolve("bwtekSetTimeUSB");

    typedef int (__stdcall *readDataFromSpectrometer)(int,int,int,int,unsigned short*,int);
    readDataFromSpectrometer readDataFromSpectrometerObj = (readDataFromSpectrometer) specLibrary.resolve("bwtekReadResultUSB");

    fCoefsA[0]=uiNameSpace::fcoeffA[currentChannel][0];
    fCoefsA[1]=uiNameSpace::fcoeffA[currentChannel][1];
    fCoefsA[2]=uiNameSpace::fcoeffA[currentChannel][2];
    fCoefsA[3]=uiNameSpace::fcoeffA[currentChannel][3];
    fCoefsB[0]=uiNameSpace::fcoeffB[currentChannel][0];
    fCoefsB[1]=uiNameSpace::fcoeffB[currentChannel][1];
    fCoefsB[2]=uiNameSpace::fcoeffB[currentChannel][2];
    fCoefsB[3]=uiNameSpace::fcoeffB[currentChannel][3];


//-------------------------------------------------
// open new file for channel
    if(uiNameSpace::isAcquiringLightNow or uiNameSpace::acquireLightOnceNow)
    {
        QString tmpFileName = uiNameSpace::spectrometerName[spectrometerIndex]+" "+uiNameSpace::sampleType+" "+dateStamp+" "+timeStamp+".Dat";

        uiNameSpace::oldFileNames[spectrometerIndex]=tmpFileName;

        outputFileName.setFileName(uiNameSpace::outputFilesDir+"/"+tmpFileName);
        if (!outputFileName.open(QFile::WriteOnly | QFile::Text))
            return;
        writeToFile.setDevice(&outputFileName);
    }

// end open new file for channel

    unsigned long threadCounter=0;
    long sleepingtime;
    unsigned short rawData[2048],rawData0[2048],rawData1[2048],rawData2[2048];
    bool printDarkSignalHeading=true;
    QTime elapsedTimer;
    uiNameSpace::graphsTimeStamp[spectrometerIndex].clear();
    unsigned long exposureReturn=0;

    emit resetPlotParameters(spectrometerIndex);

//obtain data - write data to file - update Plots
    while (!quitThread)
    {
        elapsedTimer.start();

        if(uiNameSpace::printDataHeadings[spectrometerIndex] and outputFileName.isOpen())
        {
            writeToFile << "#Date    " << "\t" << "Time" << "\t" << "Exp(ms)" << "\t" << "N" << "\t";
            for (int i=uiNameSpace::xBeginPixel[currentChannel];i<uiNameSpace::xEndPixel[currentChannel]+1;i++)
            {
                writeToFile << QString::number(fCoefsA[0]+fCoefsA[1]*i+fCoefsA[2]*qPow(i,2)+fCoefsA[3]*qPow(i,3)) << "\t";
            }
            writeToFile << endl;
            uiNameSpace::printDataHeadings[spectrometerIndex]=false;
        } //print headings of columns on the output file.

//set exposure
        if(uiNameSpace::spectrometerExposureInmSec[spectrometerIndex] != exposureReturn) //check if new exposure is assigned
            exposureReturn=setExposureTimeObj(uiNameSpace::spectrometerExposureInmSec[spectrometerIndex]*1000,currentChannel);
            if (exposureReturn<1)
                break;

//read data for from spectrometers
        uiNameSpace::graphsTimeStamp[spectrometerIndex].append(QDate::currentDate().toString("yyyy-MM-dd")+"\t"+QTime::currentTime().toString("hh:mm:ss"));

        if(!uiNameSpace::isModeMean) // this for median
        {
            int isAccomplished = readDataFromSpectrometerObj(0, uiNameSpace::scansAveraged, uiNameSpace::smoothingType, uiNameSpace::smoothingValue, &rawData0[0], currentChannel);
            if (isAccomplished<0)
            {
                if(outputFileName.isOpen())
                    writeToFile << "#could not read data at: " << uiNameSpace::graphsTimeStamp[spectrometerIndex][threadCounter];
                    break;
            }

            isAccomplished = readDataFromSpectrometerObj(0, uiNameSpace::scansAveraged, uiNameSpace::smoothingType, uiNameSpace::smoothingValue, &rawData1[0], currentChannel);
            if (isAccomplished<0)
            {
                if(outputFileName.isOpen())
                writeToFile << "#could not read data at: " << uiNameSpace::graphsTimeStamp[spectrometerIndex][threadCounter];
                break;
            }

            isAccomplished = readDataFromSpectrometerObj(0, uiNameSpace::scansAveraged, uiNameSpace::smoothingType, uiNameSpace::smoothingValue, &rawData2[0], currentChannel);
            if (isAccomplished<0)
            {
                if(outputFileName.isOpen())
                writeToFile << "#could not read data at: " << uiNameSpace::graphsTimeStamp[spectrometerIndex][threadCounter];
                break;
            }

            //find the median
            for(int i=0;i<2048;i++)
            {
                if((rawData0[i] >= rawData1[i]) and (rawData0[i] < rawData2[i]))
                {
                    rawData[i]=rawData0[i];
                }
                else if((rawData0[i] < rawData1[i]) and (rawData0[i] >= rawData2[i]))
                {
                    rawData[i]=rawData0[i];
                }

                if((rawData1[i] >= rawData0[i]) and (rawData1[i] < rawData2[i]))
                {
                    rawData[i]=rawData1[i];
                }
                else if((rawData1[i] < rawData0[i]) and (rawData1[i] >= rawData1[i]))
                {
                    rawData[i]=rawData1[i];
                }

                if((rawData2[i] >= rawData0[i]) and (rawData2[i] < rawData1[i]))
                {
                    rawData[i]=rawData2[i];
                }
                else if((rawData2[i] < rawData0[i]) and (rawData2[i] >= rawData1[i]))
                {
                    rawData[i]=rawData2[i];
                }
            }
        }
        else
        {
            int isAccomplished = readDataFromSpectrometerObj(0, uiNameSpace::scansAveraged, uiNameSpace::smoothingType, uiNameSpace::smoothingValue, &rawData[0], currentChannel);
            if (isAccomplished<0)
            {
                if(outputFileName.isOpen())
                writeToFile << "#could not read data at: " << uiNameSpace::graphsTimeStamp[spectrometerIndex][threadCounter];
                break;
            }
        } // end read data from spectrometers

        if(uiNameSpace::isAcquiringLightNow or uiNameSpace::acquireLightOnceNow)
        {
            if(printDarkSignalHeading and uiNameSpace::isBackgroundAcquired)
            {
                writeToFile << "#Dark Signal" << endl;
                writeToFile << QDate::currentDate().toString() << "\t" << QTime::currentTime().toString() <<"\t"<<uiNameSpace::scansAveraged<<"\t";
                for (int i=uiNameSpace::xBeginPixel[currentChannel];i<uiNameSpace::xEndPixel[currentChannel]+1;i++)
                {
                    writeToFile << rawData[i] << "\t";
                    uiNameSpace::spectOutput[spectrometerIndex][i]=rawData[i];
                }
                writeToFile << endl; //move to the following line
                writeToFile << "#Light Signal" << endl;
                writeToFile.flush();
                printDarkSignalHeading=false;
            }


            writeToFile << uiNameSpace::graphsTimeStamp[spectrometerIndex][threadCounter] << "\t" << uiNameSpace::spectrometerExposureInmSec[spectrometerIndex] <<"\t"<<uiNameSpace::scansAveraged<<"\t";
            if(uiNameSpace::isBackgroundNeedToBeSubtracted)
            {
                for (int i=uiNameSpace::xBeginPixel[currentChannel];i<uiNameSpace::xEndPixel[currentChannel]+1;i++)
                {
                    if(uiNameSpace::spectBackGroundSignal[spectrometerIndex][i] < rawData[i])
                    {
                        writeToFile << rawData[i]-uiNameSpace::spectBackGroundSignal[spectrometerIndex][i] << "\t";
                        uiNameSpace::spectOutput[spectrometerIndex][i]=rawData[i]-uiNameSpace::spectBackGroundSignal[spectrometerIndex][i];
                    }
                    else
                    {
                        writeToFile << 0 << "\t";
                        uiNameSpace::spectOutput[spectrometerIndex][i]=0;
                    }
                }
            }
            else
            {
                for (int i=uiNameSpace::xBeginPixel[currentChannel];i<uiNameSpace::xEndPixel[currentChannel]+1;i++)
                {
                        writeToFile << rawData[i]<< "\t";
                        uiNameSpace::spectOutput[spectrometerIndex][i]=rawData[i];
                }
            }
            writeToFile << endl; //move to the following line
            writeToFile.flush();


        }
        else if (uiNameSpace::isAcquiringDarkSignalNow)
        {
            for(int i=0;i<2048;i++)
            {
                uiNameSpace::spectBackGroundSignal[spectrometerIndex][i]=rawData[i];
                uiNameSpace::spectOutput[spectrometerIndex][i]=rawData[i];
            }
        }

        emit plotNewData(currentChannel,spectrometerIndex);

// calculate sleeping time for this cycle
        sleepingtime=uiNameSpace::readingIntervalInmSec-elapsedTimer.elapsed();
        if (sleepingtime < 0)
        {
            msleep(0);
        }
        else
        {
            msleep(sleepingtime);
        }


//qui the thread
        if(uiNameSpace::isAcquiringDarkSignalNow)
        {
            quitThread=true;
        }
            else if (uiNameSpace::isAcquiringLightNow and threadCounter >= uiNameSpace::totalRunTimeInmSec/uiNameSpace::readingIntervalInmSec)
        {
            quitThread=true;
        }
        else if(uiNameSpace::acquireLightOnceNow)
        {
            quitThread=true;
        }

        threadCounter++;
        emit updateProgressBar();
    } // end: obtain data - write data to file - update Plots

    if(outputFileName.isOpen())
    {
        QString tmpFileName = uiNameSpace::spectrometerName[spectrometerIndex]+" "+uiNameSpace::sampleType+" "+dateStamp+" "+timeStamp+".Dat";
        tmpFileName.prepend(uiNameSpace::outputFilesDir+"/");

        if(tmpFileName != outputFileName.fileName())
        {
            outputFileName.rename(tmpFileName);
        }
        uiNameSpace::oldFileNames[spectrometerIndex]=outputFileName.fileName();
        outputFileName.close();
    }

    emit quitImediately();
}


void specThreadClass::quitThreadNow()
{
    quitThread=true;
}
