#include "ui.h"
#include "mainwindow.h"
#include "toolBarclass.h"
#include "activespectrometers.h"
#include "runparametersclass.h"
#include "filtersClass.h"
#include "plotspectraclass.h"
#include "menubarclass.h"
#include "spectrometerfunctions.h"
#include <QProgressBar>
#include <QStatusBar>
#include <QToolBar>
#include <QSysInfo>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent):QMainWindow(parent)
{
    detectOSVERtoConfigSettings(); // this function changes the settings based on os version.

//SpectromtersFuntions
    bool isAccomplished = uiNameSpace::initiateSpectrometers();
    if (!isAccomplished)
    {
        statusBar()->showMessage("Problem with devices initiations");
    }
    else
    {
        statusBar()->showMessage(QString::number(uiNameSpace::noSpectrometers)+" are found and ready !");
    }


//end::spectrometers functions

    uiNameSpace::plotSpectraObj=new plotSpectraClass;

    uiNameSpace::centralWidgetObj=new QWidget(this);
    uiNameSpace::mainMenuBarObj=new menuBarClass;
    connect(uiNameSpace::mainMenuBarObj,SIGNAL(closeApplication()),this,SLOT(close()));

    this->setMenuBar(uiNameSpace::mainMenuBarObj);
    uiNameSpace::mainToolBarObj=new toolBarClass(this);
    this->addToolBar(Qt::TopToolBarArea,uiNameSpace::mainToolBarObj);
    uiNameSpace::mainStatusBarObj= new QStatusBar(this);
    uiNameSpace::mainProgressBarObj=new QProgressBar(this);
    uiNameSpace::mainProgressBarObj->setRange(0,100);
    uiNameSpace::mainProgressBarObj->setValue(0);
    uiNameSpace::mainStatusBarObj->addPermanentWidget(uiNameSpace::mainProgressBarObj);
    this->setStatusBar(uiNameSpace::mainStatusBarObj);

    QVBoxLayout *mainFormLeftVBoxLayout=new QVBoxLayout;
    QVBoxLayout *mainFormRightVBoxLayout=new QVBoxLayout;
    QHBoxLayout *mainFormHBoxLayout=new QHBoxLayout;

    mainFormLeftVBoxLayout->setSpacing(0);
    mainFormLeftVBoxLayout->setMargin(0);
    mainFormRightVBoxLayout->setSpacing(0);
    mainFormRightVBoxLayout->setMargin(0);
    mainFormHBoxLayout->setSpacing(0);
    mainFormHBoxLayout->setMargin(0);

    mainFormLeftVBoxLayout->addWidget(new runParametersClass,0);
    mainFormLeftVBoxLayout->addWidget(new activeSpectrometers,0);
    mainFormRightVBoxLayout->addWidget(new filtersClass,1);
    mainFormRightVBoxLayout->addWidget(uiNameSpace::plotSpectraObj,2);

    mainFormHBoxLayout->addLayout(mainFormLeftVBoxLayout);
    mainFormHBoxLayout->addLayout(mainFormRightVBoxLayout);
    uiNameSpace::centralWidgetObj->setLayout(mainFormHBoxLayout);
    setCentralWidget(uiNameSpace::centralWidgetObj);

    connect(uiNameSpace::mainProgressBarObj,SIGNAL(valueChanged(int)),uiNameSpace::mainToolBarObj,SLOT(progressBaris100(int)));
    connect(this,SIGNAL(assignSpectsPreferences()),uiNameSpace::mainMenuBarObj,SLOT(assignSameChannelsAtStartup()));
    emit assignSpectsPreferences();
}

MainWindow::~MainWindow()
{

uiNameSpace::shutdownSpectrometers();

}

void MainWindow::detectOSVERtoConfigSettings()
{

    if(QSysInfo::windowsVersion()==QSysInfo::WV_XP)
    {

    }
    else if (QSysInfo::windowsVersion()==QSysInfo::WV_WINDOWS7)
    {
        uiNameSpace::WinXFontS=QFont("Segoe UI",10,QFont::Normal);
        uiNameSpace::WinXFontM=QFont("Segoe UI",11,QFont::Normal);
        uiNameSpace::WinXFontL=QFont("Segoe UI",12,QFont::Normal);
    }

}
